﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Data.SQLite.Linq;
using System.Threading;
using System.Drawing;

namespace screencapture
{

    public class PointDataSQLite
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        string tableName;
        public List<FramePoint> ListPointsData;

        //пути к temp файлам
        public string pathToLogMouse;
        public string pathToLogEye;
        public string pathToLogMind;
        public string pathToLogTimeStamp;
        
        /// <summary>
        /// Создает таблицу для нового исследования в имеющейся или новой БД
        /// </summary>
        /// <param name="pathToDB">Полный путь с расширением</param>
        /// <param name="tableName">Имя таблицы\исследования</param>
        public void CreateOrUsingDB(string pathToDB, string tableName)
        {
            this.tableName = tableName;

            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);

            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, gazeX INTEGER, gazeY INTEGER, mouseX INTEGER, mouseY INTEGER, timeStamp DOUBLE, timeGazeFixed DOUBLE, cluster INTEGER, " +
                    "meditation INTEGER, attention INTEGER, alpha1 INTGER, alpha2 INETEGER, beta1 ITNEGER, beta2 INTEGER, gamma1 INTEGER, gamma2 INTEGER, poor_signal INTEGER, blink INTEGER, raw INTEGER)";
                _SQLiteCommand.ExecuteNonQuery();

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
        }

        /// <summary>
        /// Записывает имеющийся List с данными в таблицу базы данных.
        /// </summary>
        public void SerializeToBD()
        {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture; //Для запись в бд типов с плавающей точкой(а не запятой)
            lock (ListPointsData)
            {
                foreach (var point in ListPointsData)
                {
                    _SQLiteCommand.CommandText = "INSERT INTO " + tableName + " ( gazeX, gazeY, mouseX, mouseY, timeStamp, timeGazeFixed, cluster, meditation, attention, alpha1, alpha2, beta1, beta2, gamma1, gamma2, poor_signal, blink, raw ) "
                        + "VALUES('" + point.X + "', '" + point.Y + "', '" + point.mouseX + "', '" + point.mouseY + "', '" + point.timeStamp + "', '" + point.timeGazeFixed + "', '" + point.cluster + "', '" 
                        + point.meditation + "', '" + point.attention + "', '" + point.alpha1 + "', '" + point.alpha2 + "', '" + point.beta1 + "', '" + point.beta2 + "', '" + point.gamma1 + "', '" + point.gamma2 + "', '" + point.poor_signal + "', '" + point.blink + "', '" + point.raw + "'); ";
                    _SQLiteCommand.ExecuteNonQuery();
                }
            }

            //чистим temp
            //WipeLog(pathToLogMouse);
            //WipeLog(pathToLogEye);
            //WipeLog(pathToLogMind);
            //WipeLog(pathToLogTimeStamp);
        }

        /// <summary>
        /// Пишет каждый кадр сразу в базу данных
        /// </summary>
        /// <param name="tempGazeX">Абсцисса положения взгляда</param>
        /// <param name="tempGazeY">Ордината положения взгляда</param>
        /// <param name="mouseCursorX">Абсцисса положения курсора мыши</param>
        /// <param name="mouseCursorY">Ордината положения курсора мыши</param>
        /// <param name="timeCounter">Текущее время в сек, относительно начала записи</param>

        public void WritingToDB(double GazeX, double GazeY, double mouseCursorX, double mouseCursorY, double timeCounter, 
            int meditationPerc, int attentionPerc, int alpha1, int alpha2, int beta1, int beta2, int gamma1, int gamma2, int poor_signal, int blink, int raw)
        {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            _SQLiteCommand.CommandText = "INSERT INTO " + tableName + " ( gazeX, gazeY, mouseX, mouseY, timeStamp, timeGazeFixed, cluster, meditation, attention, aplha1, alpha2, beta1, beta2, gamma1, gamma2, poor_signal, blink, raw ) "
                        + "VALUES('" + (int)GazeX + "', '" + (int)GazeY + "', '" + (int)mouseCursorX + "', '" + (int)mouseCursorY + "', '" + timeCounter + "', '" + 0 + "', '" + 0 + "', '" 
                        + meditationPerc + "', '" + attentionPerc + "', '" + alpha1 + "', '" + alpha2 + "', '" + beta1 + "', '" + beta2 + "', '" + gamma1 + "', '" + gamma2 + "', '" + poor_signal + "', '" + blink + "', '" + raw + "'); ";
            _SQLiteCommand.ExecuteNonQuery();
        }

        /// <summary>
        /// Вызывается при записи кадра, добавляет в List новый элемент с координатами и временем записи кадра относительно начала записи.
        /// </summary>
        /// <param name="tempGazeX">Абсцисса положения взгляда</param>
        /// <param name="tempGazeY">Ордината положения взгляда</param>
        /// <param name="mouseCursorX">Абсцисса положения курсора мыши</param>
        /// <param name="mouseCursorY">Ордината положения курсора мыши</param>
        /// <param name="timeCounter">Текущее время в сек, относительно начала записи</param>
        /// 
        public void LogWritingTemp(bool tempRewrite, string projectDirectory, double tempGazeX, double tempGazeY, double mouseCursorX, double mouseCursorY, double timeCounter, double timeFixed, int cluster, 
            int meditation, int attention, int alpha1, int alpha2, int beta1, int beta2, int gamma1, int gamma2, int poor_signal, int blink, int raw)
        {
            lock (ListPointsData)
            {
                this.ListPointsData.Add(new FramePoint((int)tempGazeX, (int)tempGazeY, (int)mouseCursorX, (int)mouseCursorY, timeCounter, timeFixed, cluster, 
                    (int)meditation, (int)attention, (int) alpha1, (int)alpha2, (int)beta1, (int)beta2, (int)gamma1, (int)gamma2, (int)poor_signal, (int)blink, (int)raw));
            }

            //path
            //файлы для temp-лога
            //pathToLogMouse = projectDirectory + "\\temp_mouse.txt";
            //pathToLogEye = projectDirectory + "\\temp_eye.txt";
            //pathToLogMind = projectDirectory + "\\temp_mind.txt";
            //pathToLogTimeStamp = projectDirectory + "\\temp_timestamp.txt";

            //backup 
            //WriteToLog(tempRewrite, timeCounter.ToString(), pathToLogTimeStamp);
            //WriteToLog(tempRewrite, mouseCursorY.ToString() + " " + mouseCursorX.ToString(), pathToLogMouse);
            //WriteToLog(tempRewrite, tempGazeX.ToString() + " " + tempGazeY.ToString(), pathToLogEye);
            //WriteToLog(tempRewrite, attention.ToString() + " " + meditation.ToString(), pathToLogMind);

        }

        //private void WriteToLog(bool tempRewrite, string line, string pathToLog)
        //{
        //    //using (StreamWriter sw = new StreamWriter(pathToLog, tempRewrite, System.Text.Encoding.Default))
        //    //{
        //    //    sw.WriteLine(line);
        //    //}
        //}

        //private void WipeLog(string pathToLog)
        //{
        //    //System.IO.File.Delete(pathToLog);
        //}

        public void LogWriting(double tempGazeX, double tempGazeY, double mouseCursorX, double mouseCursorY, double timeCounter, double timeFixed, int cluster, int meditation, int attention, int raw)
        {
            lock (ListPointsData)
            {
                this.ListPointsData.Add(new FramePoint((int)tempGazeX, (int)tempGazeY, (int)mouseCursorX, (int)mouseCursorY, timeCounter, timeFixed, cluster, (int)meditation, (int)attention, (int)raw));
            }
        }
        
    }

}
