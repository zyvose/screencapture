﻿using Accord.Video.FFMPEG;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using screencapture.Properties;

namespace screencapture
{
    class VideoProcessor
    {
        //private string pathToFile;
        private string tableName;
        private bool writing = false;
        private bool isRecording = true;

        private VideoFileReader reader;
        private VideoFileWriter writer;

        private Dictionary<int, Pen> pen = new Dictionary<int, Pen>
        {
            { 100, new  Pen(Color.FromArgb(255, 0, 0), 5) },
            { 80, new Pen(Color.FromArgb(255, 170, 0), 5) },
            { 60, new Pen(Color.FromArgb(255, 255, 0), 5) },
            { 40, new Pen(Color.FromArgb(115, 255, 0), 5) },
            { 20, new Pen(Color.FromArgb(0, 190, 255), 5) },
            { 0, new Pen(Color.FromArgb(0, 0, 255), 5) }
        };

        Font font;
        Brush brush_Red;
        Brush brush_Blue;

        private SQLiteCommand _SQLiteCommand;
        private SQLiteConnection _SQLiteConnetion;

        private int frameID;
        private double TGF; //timeGazeFixed
        private int gazeX;
        private int gazeY;
        private int gazeMarkerSize;
        private double maxTimeFixation;

        private int mouseX;
        private int mouseY;

        private int meditationPerc;
        private int attentionPerc;

        Image cursor = Resources.cursor_yellow;

        #region Отрисовка маркера
        public void GazeMarking(string pathToFile, string pathToBD, string tableName)
        {
            this.tableName = tableName;

            font = new Font("Microsoft Sans Serif", 20F, FontStyle.Bold);

            brush_Red = new SolidBrush(Color.Red);
            brush_Blue = new SolidBrush(Color.Blue);

            maxTimeFixation = Double.Parse(ConfigurationManager.AppSettings["Max_Time_Fixation"]);
            frameID = 1;
            gazeMarkerSize = Convert.ToInt32(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);
            reader = new VideoFileReader();
            writer = new VideoFileWriter();
            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToBD + "; Synchronous=OFF;");
            _SQLiteCommand.Connection = _SQLiteConnetion;

            _SQLiteConnetion.Open();
            reader.Open(pathToFile);
            writer.Open(pathToFile.Replace(".avi", "MARKERED.avi"), reader.Width, reader.Height, 25, VideoCodec.MPEG4);

            long frameCount = reader.FrameCount;
            for (int i = 0; i < frameCount; i++)
            {
                ProcessFrame();
            }
            reader.Close();
            writer.Close();
        }

        private void ProcessFrame()
        {
            //Получаем данные о текущем кадре из БД    
            _SQLiteCommand.CommandText = "SELECT timeGazeFixed FROM " + tableName + " WHERE id=" + frameID;
            TGF = Double.Parse(_SQLiteCommand.ExecuteScalar().ToString());
            _SQLiteCommand.CommandText = "SELECT gazeX FROM " + tableName + " WHERE id=" + frameID;
            gazeX = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());
            _SQLiteCommand.CommandText = "SELECT gazeY FROM " + tableName + " WHERE id=" + frameID;
            gazeY = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());
            _SQLiteCommand.CommandText = "SELECT meditation FROM " + tableName + " WHERE id=" + frameID;
            meditationPerc = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());
            _SQLiteCommand.CommandText = "SELECT attention FROM " + tableName + " WHERE id=" + frameID;
            attentionPerc = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());

            //курсор
            _SQLiteCommand.CommandText = "SELECT mouseY FROM " + tableName + " WHERE id=" + frameID;
            mouseX = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());
            _SQLiteCommand.CommandText = "SELECT mouseX FROM " + tableName + " WHERE id=" + frameID;
            mouseY = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());

            frameID++; //для получения данных о следующем кадре
            int perc = Convert.ToInt32((TGF / maxTimeFixation) * 100); //процент от максимального времени фиксации взгляда, макс время устанавливает пользователь в настройках

            Bitmap frameImage = reader.ReadVideoFrame(); //получаем изображение кадра
            using (Graphics g = Graphics.FromImage(frameImage))
            {
                for (int i = 100; i >= 0; i -= 20)
                {
                    if (perc >= i)
                    {
                        try
                        {
                            g.DrawEllipse(pen[i], gazeX - (gazeMarkerSize / 2), gazeY - (gazeMarkerSize / 2), gazeMarkerSize, gazeMarkerSize);
                            //UpdateBD(frameID, i);
                        }
                        catch { }
                        g.DrawImage(cursor, (int)mouseX, (int)mouseY);
                        g.DrawRectangle(new Pen(brush_Red), 30, reader.Height-380, 30, 300); 
                        g.DrawRectangle(new Pen(brush_Blue), 70, reader.Height-380, 30, 300);
                        g.FillRectangle(brush_Red, 30, reader.Height - 380 + (300 - attentionPerc * 3), 30, (attentionPerc * 3));
                        g.FillRectangle(brush_Blue, 70, reader.Height - 380 + (300 - meditationPerc * 3), 30, (meditationPerc * 3));
                        break; 
                    }
                }
            }

            if (isRecording && !writing)
            {
                writing = true;
                writer.WriteVideoFrame(frameImage);
                writing = false;
            }
            frameImage.Dispose();
        }

        //private void UpdateBD(int frameID, int fixValue)
        //{
        //    _SQLiteCommand.CommandText = "UPDATE " + tableName + " SET fixedstatus = '" + fixValue + "' WHERE id =" + frameID;
        //    _SQLiteCommand.ExecuteNonQuery();
        //}
        #endregion
    }
}

