﻿using System;
using System.Collections.Generic;
using System.Text;
using Accord;
using Accord.Video.FFMPEG;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Tobii.Interaction;
using Tobii.Interaction.Framework;
using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Timer = System.Timers.Timer;
using System.Text.RegularExpressions;
using screencapture.Properties;
using System.Configuration;
using System.Threading.Tasks;
using System.Threading;
using ThinkGearNET;

namespace screencapture
{
    public class VideoRecorder
    {
        private VideoFileWriter videoWriter;
        private PointDataSQLite SQLite;
        //private PointDataXML DataXML;
        private EyeTracker eyeTracker;
        private const int intervalBetweenFrames = 32; // 1000 / fps

        private Timer frameTimer;
        DateTime startVideoTime;

        private bool isRecording;
        private bool writing = false;

        string pathToDescription;
        string descriptionText;
        public string backupPath;

        //дозапись/перезапись temp файлов
        bool tempRewrite;

        double mouseX, mouseY, tempGazeX, tempGazeY, timer;
        int gazeMarkerSize;

        //NeuroSky Values
        int ns_attention;
        int ns_meditation;

        int ns_alpha_1;
        int ns_alpha_2;

        int ns_beta_1;
        int ns_beta_2;

        int ns_gamma_1;
        int ns_gamma_2;

        int ns_blink;
        int poor_signal = 200;

        int ns_raw;

        private System.Drawing.Size screenSize;
        Pen gazeMarkerpPen = new Pen(Color.FromArgb(0, 124, 173), 5);
        Image cursor = Resources.cursor_yellow;

        ThinkGearWrapper thinkGear;

        /// <summary>
        /// Конструктор
        /// </summary>
        public VideoRecorder()
        {
            eyeTracker = EyeTracker.GetEyeTracker();
            videoWriter = new VideoFileWriter();

            //thinkGear FIX
            thinkGear = new ThinkGearWrapper();

            //Получение границ экрана
            System.Drawing.Rectangle bounds = Screen.PrimaryScreen.Bounds;
            screenSize = new System.Drawing.Size(bounds.Width, bounds.Height);

            //Таймер запускающий запись и редактирование кадра с заданой частотой(FrameRate)
            frameTimer = new Timer(intervalBetweenFrames);
            frameTimer.Elapsed += ProcessFrame;
            frameTimer.AutoReset = true;

            isRecording = false;
        }

        /// <summary>
        /// Начинает запись, получая путь к файлу
        /// </summary>
        /// <param name="projectDirectory">Путь к папке с проектом</param>
        /// <param name="pathToFILE">Полное имя видеозаписи</param>
        /// <param name="nameResearch">Имя исследования</param>
        /// <param name="description">Текст описания исследования</param>
        public void StartRecording(string projectDirectory, string pathToFILE, string nameResearch, string description)
        {

            gazeMarkerSize = Convert.ToInt32(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);

            string pathToVIDEO = pathToFILE + ".avi";
            pathToDescription = pathToFILE + ".txt";
            string pathToBD = Path.Combine(projectDirectory, Path.GetFileName(projectDirectory) + ".sqlite");

            tempRewrite = false;

            this.descriptionText = description;

            //Получение потока координат с айтрекера
            eyeTracker.StartEyeStream();

            //Создание/открытие БД и таблица с логом
            SQLite = new PointDataSQLite();
            SQLite.ListPointsData = new List<FramePoint>(); //инциализирую лист
            SQLite.CreateOrUsingDB(pathToBD, nameResearch.Replace(" ", string.Empty));

            //Старт записи кадров
            isRecording = true;
            videoWriter.Open(pathToVIDEO, screenSize.Width, screenSize.Height, 25, VideoCodec.MPEG4);
            frameTimer.Start();
            startVideoTime = DateTime.Now;

            backupPath = projectDirectory;
        }

        void thinkGear_ThinkGearChanged(object sender, ThinkGearChangedEventArgs e)
        {
            
            ns_attention = Convert.ToInt32(e.ThinkGearState.Attention);
            ns_meditation = Convert.ToInt32(e.ThinkGearState.Meditation);

            ns_alpha_1 = Convert.ToInt32(e.ThinkGearState.Alpha1);
            ns_alpha_2 = Convert.ToInt32(e.ThinkGearState.Alpha2);

            ns_beta_1 = Convert.ToInt32(e.ThinkGearState.Beta1);
            ns_beta_2 = Convert.ToInt32(e.ThinkGearState.Beta2);

            ns_gamma_1 = Convert.ToInt32(e.ThinkGearState.Gamma1);
            ns_gamma_2 = Convert.ToInt32(e.ThinkGearState.Gamma2);

            ns_raw = Convert.ToInt32(e.ThinkGearState.Raw);

            poor_signal = Convert.ToInt32(e.ThinkGearState.PoorSignal);

            ns_blink = Convert.ToInt32(e.ThinkGearState.BlinkStrength);

            //Thread.Sleep(10);
        }

        public bool ThinkGearStart(){
            string defaultPort = ConfigurationManager.AppSettings["COM_Port"];
            thinkGear.ThinkGearChanged += thinkGear_ThinkGearChanged;
            thinkGear.Connect(defaultPort, ThinkGear.BAUD_57600, true); //подключаемся

            thinkGear.EnableBlinkDetection(true); //тест записи моргания

            while (true)
            {
                if ((poor_signal == 0) & (ns_attention != 0) & (ns_meditation !=0))
                    return true;
            }
        }

        public void ThinkGearStop()
        {
            thinkGear.Disconnect();
        }

        /// <summary>
        /// Безопастно заканчивает запись, создает лог, описание txt, останавливает поток с айтрекера
        /// </summary>
        public void EndRecording()
        {
            isRecording = false;
            while (writing) { }
            frameTimer.Stop();
            videoWriter.Close();
            eyeTracker.StopEyeStream();

            //thinkGear
            thinkGear.ThinkGearChanged -= thinkGear_ThinkGearChanged;
            thinkGear.Disconnect();

            CreateDescriptionFile(descriptionText, pathToDescription);
            SQLite.SerializeToBD(); //сериализация в БД

        }
        
        /// <summary>
        /// Создание описания к видеозаписи в том же каталоге в .txt
        /// </summary>
        private void CreateDescriptionFile(string descriptionText, string pathToDescription)
        {
            StreamWriter description = new StreamWriter(pathToDescription, false, Encoding.Unicode);
            description.Write(descriptionText);
            description.Close();
        }
        
        /// <summary>
        /// Наложение на кадр маркеров отображающих положение взгляда и курсора и запись кадра
        /// </summary>        
        private void ProcessFrame(object source, System.Timers.ElapsedEventArgs e)
        {
            tempGazeX = eyeTracker.gazeX;
            tempGazeY = eyeTracker.gazeY;
            mouseX = Cursor.Position.X;
            mouseY = Cursor.Position.Y;
            timer = (DateTime.Now - startVideoTime).TotalSeconds;

            Bitmap frameImage = new Bitmap(screenSize.Width, screenSize.Height);

            using (Graphics g = Graphics.FromImage(frameImage))
            {
                g.CopyFromScreen(0, 0, 0, 0, screenSize, CopyPixelOperation.SourceCopy);
            }

            if (isRecording && !writing)
            {
                writing = true;

                SQLite.LogWritingTemp(tempRewrite, backupPath, tempGazeX, tempGazeY, mouseX, mouseY, timer, 0, 0, ns_meditation, ns_attention,
                    ns_alpha_1, ns_alpha_2, ns_beta_1, ns_beta_2, ns_gamma_1, ns_gamma_2, poor_signal, ns_blink, ns_raw); //LogWriting - писать сначало в list, нужно раскоментить выше сериализацию и инициализацию List //отключаю запись
                tempRewrite = true;

                videoWriter.WriteVideoFrame(frameImage);
                writing = false;
            }
            
            frameImage.Dispose();
        }
        
    }

}
