﻿using System.Collections.Generic;

namespace screencapture
{
    public enum ResearchType
    {
        NoType = 0,
        LocalFile = 1,
        Website = 2,
        Naming = 3
    }

    public enum IsRecording
    {
        No = 0,
        Yes = 1
    }
          
}
