﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace screencapture
{
    /// <summary>
    /// Для сериализации полученых данных в XML (Не используется)
    /// </summary>
    [Serializable]
    public class PointDataXML
    {
        public List<PointDataXML> arrayPointsData;
        XmlSerializer formatter = new XmlSerializer(typeof(List<PointDataXML>));

        [XmlAttribute]
        public int gazeX { get; set; }
        [XmlAttribute]
        public int gazeY { get; set; }
        [XmlAttribute]
        public int mouseX { get; set; }
        [XmlAttribute]
        public int mouseY { get; set; }
        [XmlAttribute]
        public double timeStamp { get; set; }
        [XmlAttribute]
        public double timeFixed { get; set; }
        [XmlAttribute]
        public int cluster { get; set; }
        [XmlAttribute]
        public int meditation { get; set; }
        [XmlAttribute]
        public int attention { get; set; }


        public PointDataXML() { }

        private PointDataXML(int gazeY, int gazeX, int mouseY, int mouseX, double timeStamp, double timeFixed, int cluster, int meditation, int attention)
        {
            this.gazeY = gazeY;
            this.gazeX = gazeX;
            this.mouseY = mouseY;
            this.mouseX = mouseX;
            this.timeStamp = timeStamp;
            this.timeFixed = timeFixed;
            this.cluster = cluster;
            this.meditation = meditation;
            this.attention = attention;
        }


        public void LogWriting(double tempGazeX, double tempGazeY, double mouseCursorX, double mouseCursorY, double timeCounter, double timeFixed, int cluster, int meditation, int attention)
        {
            arrayPointsData.Add(new PointDataXML((int)tempGazeX, (int)tempGazeY, (int)mouseCursorX, (int)mouseCursorY, timeCounter, timeFixed, cluster, meditation, attention));
        }

        public void LogSerialize(string fileNameLog)
        {
            using (FileStream fs = new FileStream(fileNameLog, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, arrayPointsData);
            }
        }

    }
}
