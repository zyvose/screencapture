﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace screencapture
{
    public struct FramePoint
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int cluster { get; set; }
        public int mouseX { get; set; }
        public int mouseY { get; set; }
        public double timeStamp { get; set; }
        public double timeGazeFixed { get; set; }
        public int meditation { get; set; }
        public int attention { get; set; }

        public int alpha1 { get; set; }
        public int alpha2 { get; set; }

        public int beta1 { get; set; }
        public int beta2 { get; set; }

        public int gamma1 { get; set; }
        public int gamma2 { get; set; }

        public int poor_signal { get; set; }
        public int blink { get; set; }

        public int raw { get; set; }

        public FramePoint(int gazeX, int gazeY, int mouseY = 0, int mouseX = 0, double timeStamp = 0, double timeGazeFixed = 0, int cluster = 0, 
            int meditation = 0, int attention = 0, int alpha1 = 0, int alpha2 = 0, int beta1 = 0, int beta2 = 0, int gamma1 = 0, int gamma2 = 0, int poor_signal = 200, int blink = 0, int raw=0)
        {
            this.X = gazeX;
            this.Y = gazeY;
            this.cluster = cluster;
            this.mouseY = mouseY;
            this.mouseX = mouseX;
            this.timeStamp = timeStamp;
            this.timeGazeFixed = timeGazeFixed;
            this.meditation = meditation;
            this.attention = attention;
            this.alpha1 = alpha1;
            this.alpha2 = alpha2;
            this.beta1 = beta1;
            this.beta2 = beta2;
            this.gamma1 = gamma1;
            this.gamma2 = gamma2;
            this.poor_signal = poor_signal;
            this.blink = blink;
            this.raw = raw;
        }

        //public static FramePoint operator +(FramePoint a, FramePoint b)
        //{
        //    return new FramePoint(a.X + b.X, a.Y + b.Y);
        //}
        //public static FramePoint operator -(FramePoint a, FramePoint b)
        //{
        //    return new FramePoint(Math.Abs(a.X - b.X), Math.Abs(a.Y - b.Y));
        //}


        //public static bool operator >(FramePoint a, FramePoint b)
        //{
        //    return (a.X + a.Y) > (b.X + b.Y);
        //}
        //public static bool operator <(FramePoint a, FramePoint b)
        //{
        //    return (a.X + a.Y) < (b.X + b.Y);
        //}


        //public static bool operator >=(FramePoint a, int b)
        //{
        //    return (a.X >= b) && (a.Y >= b);
        //}
        //public static bool operator <=(FramePoint a, int b)
        //{
        //    return (a.X <= b) && (a.Y <= b);
        //}
    }
}
