﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;

namespace screencapture
{
    class TargetAudience
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        private string pathToDB = Environment.CurrentDirectory + @"\data\global.sqlite";
        private string tableName = "target_audience";

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public TargetAudience() { }

        private TargetAudience(int id, string name, string description)
        {
            this.id = id;
            this.name = name;
            this.description = description;
        }

        private void LoadFromDB()
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, gazeX INTEGER, gazeY INTEGER, mouseX INTEGER, mouseY INTEGER, timeStamp DOUBLE, timeGazeFixed DOUBLE, cluster INTEGER, " +
                    "meditation INTEGER, attention INTEGER, alpha1 INTGER, alpha2 INETEGER, beta1 ITNEGER, beta2 INTEGER, gamma1 INTEGER, gamma2 INTEGER, poor_signal INTEGER, blink INTEGER)";
                _SQLiteCommand.ExecuteNonQuery();

            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
        }
    }
}
