﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThinkGearNET;
using System.IO.Ports;
using screencapture.Properties;


namespace screencapture
{
    public partial class FormSetting : Form
    {
        Pen gazeMarkerpPen = new Pen(Color.FromArgb(0, 124, 173), 5);
        int gazeMarkerSize = Convert.ToInt32(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);

        VideoRecorder recoder;
        int size;

        public static ThinkGearWrapper thinkGear = new ThinkGearWrapper(); //шапка
        public static string activePort;
        public static bool searchStatus;
        public static bool connectStatus;
        public static bool activeStatus;

        public FormSetting(VideoRecorder recoder)
        {
            this.recoder = recoder;
            InitializeComponent();
        }

        #region Настройка директории для проектов
        private void btnSetDefaultDirectory_Click(object sender, EventArgs e)
        {
            tbPathToDefaultDir.Text = Path.Combine(Directory.GetCurrentDirectory(), "project");
        }

        private void btnSelectDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBDdir = new FolderBrowserDialog();
            FBDdir.SelectedPath = ConfigurationManager.AppSettings["Default_Project_Directory"];
            if (FBDdir.ShowDialog() == DialogResult.OK)
            {
                tbPathToDefaultDir.Text = FBDdir.SelectedPath;
            }
        }
        #endregion
        
        #region Настройка маркера взгляда на обработаном видео           
        private void trackBarSizeMarkerGaze_Scroll(object sender, EventArgs e)
        {
            size = trackBarSizeMarkerGaze.Value;
            using (Graphics g = this.CreateGraphics())
            {
                g.Clear(Color.FromName("Control"));
                g.DrawEllipse(gazeMarkerpPen, 370 - (size / 2), 200 - (size / 2), size, size);
            }
            labelGMdiametr.Text = trackBarSizeMarkerGaze.Value.ToString() + " px";
        }
        #endregion

        #region Стартовые параметры, принять\отменить
        private void FormSetting_Load(object sender, EventArgs e)
        {
            //Инициализация имеющихся настроек из app.config
            tbPathToDefaultDir.Text = ConfigurationManager.AppSettings["Default_Project_Directory"];
            trackBarSizeMarkerGaze.Value = Int32.Parse(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);
            tbMaxTimeGaze.Text = ConfigurationManager.AppSettings["Max_Time_Fixation"];
            labelGMdiametr.Text = trackBarSizeMarkerGaze.Value.ToString() + " px";

            foreach (string port in SerialPort.GetPortNames())
                cboPort.Items.Add(port);
            try { cboPort.SelectedIndex = 0; }
            catch { }
            

            string defaultPort = ConfigurationManager.AppSettings["COM_Port"];

            for (int i = 0; i < cboPort.Items.Count; i++)
            {
                if (cboPort.Items[i].ToString() == defaultPort)
                    cboPort.SelectedItem = cboPort.Items[i];
            }

            infolabel.Visible = true;
            if (defaultPort != cboPort.SelectedItem.ToString())
                infolabel.Text = "Сохраненный порт не найден";
            else
                infolabel.Text = "Сохраненный порт найден";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            //Изменение насроек в app.config
            Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["Size_Gaze_Marker"].Value = trackBarSizeMarkerGaze.Value.ToString();
            currentConfig.AppSettings.Settings["Default_Project_Directory"].Value = tbPathToDefaultDir.Text;
            currentConfig.AppSettings.Settings["Max_Time_Fixation"].Value = tbMaxTimeGaze.Text;
            if(currentConfig.AppSettings.Settings["COM_Port"] == null)
                currentConfig.AppSettings.Settings.Add("COM_Port", cboPort.SelectedItem.ToString());
            else
                currentConfig.AppSettings.Settings["COM_Port"].Value = cboPort.SelectedItem.ToString();
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            this.Close();
        }

        private void SearchPort(object state) //автопоиск порта
        {
            SynchronizationContext uiContext = state as SynchronizationContext;

            searchStatus = false;
            activeStatus = true;
            activePort = null;

            foreach (string port in SerialPort.GetPortNames())
            {
                thinkGear.Connect(port, ThinkGear.BAUD_57600, true);

                int idx = 0;

                while (idx < 101 & !searchStatus)
                {
                    thinkGear.UpdateState();
                    uiContext.Post(UpdateUI, port + ": " + idx.ToString() + "%");
                    if (thinkGear.ThinkGearState.PoorSignal > 0)
                    {
                        searchStatus = true;
                        activePort = port;
                    }

                    Thread.Sleep(80);
                    idx++;
                }

                thinkGear.Disconnect();

            }

            activeStatus = false;

            if (activePort == null)
            {
                uiContext.Post(UpdateUI, "Не найдено, попробуйте снова");
            }
            else
            {
                uiContext.Post(UpdateUI, "Успешно: " + activePort);
                uiContext.Post(UpdateSelector, activePort);
            }
            
        }

        private void UpdateSelector(object state)
        {
            for (int i = 0; i < cboPort.Items.Count; i++)
            {
                if (cboPort.Items[i].ToString() == (string)state)
                {
                    cboPort.SelectedItem = cboPort.Items[i];
                }
            }

        }

        private void UpdateUI(object state)
        {
            infolabel.Text = (string)state;
            if (!activeStatus)
            {
                activeStatus = false;
                searchButton.Enabled = true;
                connectButton.Enabled = true;
                connectButton.Text = "Подключиться";
            }

        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if (!activeStatus)
            {
                SynchronizationContext uiContext = SynchronizationContext.Current;
                Thread searchThread = new Thread(SearchPort);
                searchThread.Start(uiContext);
                searchButton.Enabled = false;
                connectButton.Enabled = false;
                connectButton.Text = "Идет поиск";
            }

        }

        #endregion

        private void searchButton_Click_1(object sender, EventArgs e)
        {
            if (!activeStatus)
            {
                SynchronizationContext uiContext = SynchronizationContext.Current;
                Thread searchThread = new Thread(SearchPort);
                searchThread.Start(uiContext);
                searchButton.Enabled = false;
                connectButton.Enabled = false;
                connectButton.Text = "Идет поиск";
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (!connectStatus)
            {

                connectStatus = true;
                thinkGear.ThinkGearChanged += thinkGear_ThinkGearChanged;
                thinkGear.Connect(cboPort.SelectedItem.ToString(), ThinkGear.BAUD_57600, true);


                SetUI(false);
            }
            else
            {

                connectStatus = false;
                thinkGear.ThinkGearChanged -= thinkGear_ThinkGearChanged;
                thinkGear.Disconnect();
                
                SetUI(true);
            }
        }

        private void SetUI(bool status)
        {
            if (status)
            {
                connectButton.Text = "Подключиться";
                infolabel.Text = "Отключено";
            }
            else
            {
                infolabel.Text = "Подключено";
                connectButton.Text = "Отключиться";
            }
            searchButton.Enabled = status;
            cboPort.Enabled = status;
        }

        void thinkGear_ThinkGearChanged(object sender, ThinkGearChangedEventArgs e) //событие на обновление данных с шапки
        {
            BeginInvoke(new MethodInvoker(delegate
            {
                SynchronizationContext uiContext = SynchronizationContext.Current;

                //attention - вот по идее это и есть контроллируемый головой параметр
                attentionVal.Text = e.ThinkGearState.Attention.ToString() + "%";
                attentionBar.Value = Convert.ToInt32(e.ThinkGearState.Attention.ToString());

                //meditation
                meditationVal.Text = e.ThinkGearState.Meditation.ToString() + "%";
                meditationBar.Value = Convert.ToInt32(e.ThinkGearState.Meditation.ToString());

                switch (e.ThinkGearState.PoorSignal)
                {
                    case 0:
                        connectBox.Image = Resources.connected;
                        break;
                    case 200:
                        connectBox.Image = Resources.nosignal;
                        break;
                    default:
                        connectBox.Image = Resources.wait;
                        break;
                }

            }));
            Thread.Sleep(10);
        }

        private void FormSetting_FormClosed(object sender, FormClosedEventArgs e)
        {
            thinkGear.Disconnect();
        }
    }
}
