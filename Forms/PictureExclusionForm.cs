﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class PictureExclusionForm : Form
    {
        string pathToDirectory;
        string[] files;
        string headline = "Тестовый заголовок";
        int pictureWidth=400;
        int pictureHeight=400;
        int pictureMargin = 10;
        List<Label> helpList = new List<Label>();
        List<Label> headerList = new List<Label>();
        List<PictureBox> pictureList = new List<PictureBox>();
        Dictionary<int, string> helplines = new Dictionary<int, string>();
        public PictureExclusionForm()
        {
            InitializeComponent();
        }
        private void PictureExclusionForm_Load(object sender, EventArgs e)
        {
            pathToDirectory = "d:\\Брендмашина\\screencapture\\screencapture\\bin\\Debug\\project\\stimulatorPictures";
            this.WindowState = FormWindowState.Maximized;
            helplines.Add(0, "Кликните на изображение, которое хотите исключить");
            //CreateHeadlineLabels();
            files = Directory.GetFiles(pathToDirectory);
            CreateHeadlineLabels();
            CreatePictureObjects(files);

            ShowRebuildTask("Сейчас вам предстоит выбрать из представленных на экране вариантов те, которые нужно исключить из рассмотрения.", "Продолжить");
            
            
            
        }
        private void CreateHeadlineLabels() //создает хедеры
        {
            for (int i = 0; i < 2; i++)
            {
                CreateHeader(32, i);
            }
        }
        private void nextButton_Click(object sender, EventArgs e)
        {
            if (nextButton.Text == "ОК")
            {
                this.Close();
            }
            else
            {
                foreach (PictureBox currentPicture in pictureList)
                {
                    currentPicture.Visible = true;
                }


                descPanel.Visible = false;
                HelpVisible(true);
            }
        }
        private void HelpVisible(bool visible) //визибл подсказок
        {
            foreach (Label currentWord in helpList)
            {
                currentWord.Visible = visible;
            }
            foreach (Label currentWord in headerList)
            {
                currentWord.Visible = visible;
            }
        }
        private void CreateHeader(int textSize, int position) //выводит хедеры на форму
        {
            Label header = new Label();
            header.Text = headline;
            header.Font = new Font("Arial", textSize);
            header.Width = this.ClientSize.Width;
            header.TextAlign = ContentAlignment.TopCenter;
            header.Visible = false;
            header.Size = new Size(header.Size.Width, Convert.ToInt32(textSize * 1.56));
            if (position < 1)
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.02));
            }
            else
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - header.Size.Height));
            }

            CreateHelpLines(18, header.Location.Y, position);
            headerList.Add(header);

            this.Controls.Add(header);
        }
        private void CreateHelpLines(int textSize, int position, int mode) //создает и выводит на форму подсказки
        {
            Label help = new Label();
            help.Text = helplines[0]; //исправить 
            help.Font = new Font("Arial", textSize);
            help.Width = this.ClientSize.Width;
            help.TextAlign = ContentAlignment.TopCenter;
            help.Size = new Size(help.Size.Width, Convert.ToInt32(textSize * 1.56));
            help.ForeColor = Color.Gray;

            if (mode < 1)
                help.Location = new Point(0, position + help.Size.Height * 2);
            else
                help.Location = new Point(0, position - help.Size.Height);

            helpList.Add(help);
            this.Controls.Add(help);
        }
        private void ShowRebuildTask(string message, string button) //вызов окна с указанием для юзера
        {
            HelpVisible(false);

            foreach (PictureBox currentPicture in pictureList)
            {
                currentPicture.Visible = false;
            }

            descriptionLabel.Text = message;
            nextButton.Text = button;
            descPanel.Location = new Point((this.ClientSize.Width - descPanel.Size.Width) / 2, (this.ClientSize.Height - descPanel.Size.Height) / 2);
            descPanel.Visible = true;
        }
        private void RemovePictureObject(object sender, EventArgs e) //событие при клике на кнопку, удаляющее картинку и запускающее реролл 
        {
            if (pictureList.Count > 1)
            {
                foreach (PictureBox currentPicture in pictureList)
                {
                    if (sender == currentPicture)
                    {
                        pictureList.Remove(currentPicture);
                        currentPicture.Dispose();
                        break;
                    }
                }

                RandomPictureList(pictureList);
                foreach (PictureBox currentPic in pictureList)
                {
                    currentPic.Visible = true;
                }
            }
            else
            {
                ShowRebuildTask("Исследование завершено.\nВыполнить расчет и сохранить данные.", "ОК");
            }
        }
        private void CreatePictureObjects(string[] pathToDirectory)
        {
            for (int i = 0; i < pathToDirectory.Length; i++)
            {
                pictureList.Add(new PictureBox());
                pictureList[pictureList.Count - 1].Image =Image.FromFile(files[i]);
                pictureList[pictureList.Count - 1].Height = pictureHeight;
                pictureList[pictureList.Count - 1].Width = pictureWidth;
                pictureList[pictureList.Count - 1].Cursor = Cursors.Hand;
                pictureList[pictureList.Count - 1].SizeMode = PictureBoxSizeMode.StretchImage;
                pictureList[pictureList.Count - 1].Visible = false;
                pictureList[pictureList.Count - 1].Click+= new EventHandler(RemovePictureObject);
            }
            RandomPictureList(pictureList);

        }
        private void RandomPictureList(List<PictureBox> pictureList)
        {
            Random Rand = new Random();

            for (int i = 0; i < pictureList.Count; i++)
            {
                PictureBox tmp = pictureList[0];
                pictureList.RemoveAt(0);
                pictureList.Insert(Rand.Next(pictureList.Count), tmp);
            }
            SetPositionPictureObjects(pictureList);
        }
        private void setHelpLinesPosition(List<Label> headerList, List<Label> helpList) //корректирует позиции подсказок
        {
            foreach (Label currentWord in headerList)
            {
                currentWord.Width = this.ClientSize.Width;
            }
            foreach (Label currentWord in helpList)
            {
                currentWord.Width = this.ClientSize.Width;
            }

            try
            {
                headerList[headerList.Count - 1].Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - headerList[headerList.Count - 1].Size.Height));
                helpList[helpList.Count - 1].Location = new Point(0, headerList[headerList.Count - 1].Location.Y - helpList[helpList.Count - 1].Size.Height);
            }
            catch { }
        }
        private void SetPositionPictureObjects(List<PictureBox> pictureList) //расставляет слова по форме (нужно избавляться от констант)
        {
            int leftPadding = Convert.ToInt32(this.ClientSize.Width * 0.1);
            int topPadding = 100;

            int x = leftPadding;
            int y = topPadding;

            foreach (PictureBox currentPicture in pictureList)
            {
                currentPicture.Visible = false;
                currentPicture.Location = new Point(x, y);
                this.Controls.Add(currentPicture);
                if (x + pictureMargin + leftPadding + currentPicture.Size.Width * 2 < this.ClientSize.Width)
                    x = x + currentPicture.Size.Width + pictureMargin;
                else
                {
                    x = leftPadding;
                    y = y + currentPicture.Size.Height + 10;
                }
            }

            //CenterLabelAlignHorizontal(labelList);
            //CenterLabelAlignVertical(labelList);
        }
        private void CenterPictureAlignHorizontal(List<PictureBox> pictureList) //центрует по горизонтали
        {
            if (pictureList.Count > 0)
            {
                PictureBox currentPicture = pictureList[0];
                int id = 0;

                foreach (PictureBox currentWord in pictureList)
                {
                    if (((id + 1) == pictureList.Count) || (pictureList[id + 1].Location.Y != currentPicture.Location.Y))
                    {
                        int newLocation = (Convert.ToInt32(this.ClientSize.Width - ((pictureList[id].Location.X + pictureList[id].Size.Width) - currentPicture.Location.X)) / 2) - currentPicture.Location.X;
                        for (int i = pictureList.IndexOf(currentPicture); i <= id; i++)
                        {
                            pictureList[i].Location = new Point(pictureList[i].Location.X + newLocation, pictureList[i].Location.Y);
                        }
                        if ((id + 1) != pictureList.Count)
                            currentPicture = pictureList[id + 1];
                    }
                    id++;
                }
            }
        }
        /*private void CenterLabelAlignVertical(List<Label> labelList) //центрует по вертикали
        {
            try
            {
                int sizeHeigh = labelList[labelList.Count - 1].Location.Y - labelList[0].Location.Y + labelList[labelList.Count - 1].Size.Height;

                if (sizeHeigh >= this.ClientSize.Height * 0.75)
                {
                    SizingLabelText(labelList, Convert.ToInt32(labelList[0].Font.Size) - 2);
                }
                else
                {
                    if ((sizeHeigh < this.ClientSize.Height * 0.5) & !(sizeHeigh > this.ClientSize.Height * 0.75) & (labelList[0].Font.Size < 40))
                    {
                        SizingLabelText(labelList, Convert.ToInt32(labelList[0].Font.Size) + 2);
                    }
                    else
                    {
                        int diffHeigh = Convert.ToInt32((this.ClientSize.Height - sizeHeigh) / 2) - labelList[0].Location.Y;

                        foreach (Label currentWord in labelList)
                        {
                            currentWord.Location = new Point(currentWord.Location.X, currentWord.Location.Y + diffHeigh);
                            currentWord.Visible = true;
                        }
                        //WriteCoordinats(labelList);
                    }
                }
            }
            catch { }
        }
        */
        private void PictureExclusionForm_Resize(object sender, EventArgs e) //устанавливает позицию для слов и подсказок при резайзе формы
        {
            SetPositionPictureObjects(pictureList);
            setHelpLinesPosition(headerList, helpList);
        }

    }
}
