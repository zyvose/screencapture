﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class StimulatorPicturesForm : Form
    {
        
        string FIO;
        string pathToDirectory;
        string headline = "Изображения-стимуляторы";
        int showPictureTime;
        int delayTime;
        int opacityTimeIn=1000;
        int opacityTimeOut=1000;
        float fpsIn=10;
        float fpsOut=10;
        int opacityError = 0;
        float currentOpacity=0;
        List<Label> headerList = new List<Label>();
        int i;
        string[] files;
        Image currentImage;
        Dictionary<int, string> helplines = new Dictionary<int, string>();

        
        public StimulatorPicturesForm(int showPictureTime, int delayTime, string FIO)
        {
            this.FIO = FIO;
            this.showPictureTime = showPictureTime*1000;
            this.delayTime = delayTime*1000;
            if (delayTime == 0)
            {
                this.delayTime = 1;
            }
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (startButton.Text == "OK")
            {
                this.Close();
            }
            else
            {
                
                startButton.Visible = false;
                descriptionLabel.Visible = false;
                timerPictures.Interval = showPictureTime;
                timerDelay.Interval = delayTime;
                timerOpacityIn.Interval = (int)(opacityTimeIn/fpsIn);
                timerOpacityOut.Interval = (int)(opacityTimeOut / fpsOut);

                files = Directory.GetFiles(pathToDirectory);
                pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                
                pictureBox.Visible = true;
                showPicture(files[i]);
                
            }
        }

        private void StimulatorPicturesForm_Load(object sender, EventArgs e)// Загружает путь к папке с картинками
        {
            pathToDirectory = "d:\\Брендмашина\\screencapture\\screencapture\\bin\\Debug\\project\\stimulatorPictures";
            this.WindowState = FormWindowState.Maximized;
            
            CreateHeader(32, 1);

        }
        private void CreateHeader(int textSize, int position) //выводит хедеры на форму
        {
            Label header = new Label();
            header.Text = headline;
            header.Font = new Font("Arial", textSize);
            header.Width = this.ClientSize.Width;
            header.TextAlign = ContentAlignment.TopCenter;
            header.Visible = false;
            header.Size = new Size(header.Size.Width, Convert.ToInt32(textSize * 1.56));
            if (position < 1)
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.02));
            }
            else
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - header.Size.Height));
            }

            
            headerList.Add(header);

            this.Controls.Add(header);
        }
        private void SetPictureBoxPosition()
        {
            Size size = new Size(this.ClientSize.Width * 8 / 10, this.ClientSize.Height * 8 / 10);
            pictureBox.Size = size;
            int y = this.ClientSize.Height/10;
            int x = this.ClientSize.Width / 10;
            pictureBox.Location = new Point(x,y);


        }
        private void timerPictures_Tick(object sender, EventArgs e)
        {
            pictureBox.Visible = true;
            timerPictures.Stop();
            hidePicture(files[i]);
            
        }
        private void setHelpLinesPosition(List<Label> headerList) //корректирует позиции подсказок
        {
            foreach (Label currentWord in headerList)
            {
                currentWord.Width = this.ClientSize.Width;
            }

            try
            {
                headerList[headerList.Count - 1].Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - headerList[headerList.Count - 1].Size.Height));
                
            }
            catch { }
        }
        private void StimulatorPicturesForm_Resize(object sender, EventArgs e) //устанавливает позицию для слов и подсказок при резайзе формы
        {
            SetPictureBoxPosition();
            setHelpLinesPosition(headerList);
            descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width/2, 5);
            startButton.Location = new Point(this.ClientSize.Width / 2 - startButton.Width/2, this.ClientSize.Height - startButton.Height - 5);
        }
        private void timerDelay_Tick(object sender, EventArgs e)
        {
            timerDelay.Stop();
            pictureBox.Visible = true;
            showPicture(files[i]);
            
        }
        private void showPicture(string path)
        {
            pictureBox.Visible = true;
            timerOpacityIn.Start();
            
                        
        }
        private void hidePicture(string path)
        {
            pictureBox.Visible = true;
            timerOpacityOut.Start();


        }
        public static Image SetImgOpacity(Image imgPic, float imgOpac)

        {

            Bitmap bmpPic = new Bitmap(imgPic.Width, imgPic.Height);
            Graphics gfxPic = Graphics.FromImage(bmpPic);
            ColorMatrix cmxPic = new ColorMatrix();
            cmxPic.Matrix33 = imgOpac;
            ImageAttributes iaPic = new ImageAttributes();
            iaPic.SetColorMatrix(cmxPic, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            gfxPic.DrawImage(imgPic, new Rectangle(0, 0, bmpPic.Width, bmpPic.Height), 0, 0, imgPic.Width, imgPic.Height, GraphicsUnit.Pixel, iaPic);
            gfxPic.Dispose();
            
            return bmpPic;

        }
        public static Bitmap ChangeOpacity(Image img, float opacityvalue)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height); // Determining Width and Height of Source Image
            Graphics graphics = Graphics.FromImage(bmp);
            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.Matrix33 = opacityvalue;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            graphics.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();   // Releasing all resource used by graphics 
            return bmp;
        }

        private void timerOpacityIn_Tick(object sender, EventArgs e)
        {
            currentOpacity += 1000 / fpsIn /opacityTimeIn ;
            pictureBox.Image = ChangeOpacity(Image.FromFile(files[i]), currentOpacity);
            if (currentOpacity >= 1)
            {
                timerOpacityIn.Stop();
                currentOpacity = 1;
                timerPictures.Start();
            }
        }

        private void timerOpacityOut_Tick(object sender, EventArgs e)
        {
            currentOpacity -= 1000/fpsOut/opacityTimeOut;
            pictureBox.Image = ChangeOpacity(Image.FromFile(files[i]), currentOpacity);
            if (currentOpacity <= 0)
            {
                timerOpacityOut.Stop();
                currentOpacity = 0;
                pictureBox.Visible = false;
                i++;
                if (i < files.Length)
                {
                    
                    timerDelay.Start();
                }
                else
                {
                    descriptionLabel.Text = "Исследование завершено";
                    startButton.Text = "OK";
                    descriptionLabel.Visible = true;
                    startButton.Visible = true;
                }
                
            }
        }
    }
}
