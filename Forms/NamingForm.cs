﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace screencapture.Forms
{
    public partial class NamingForm : Form
    {
        //временно, нужно будет передавать/брать общий
        private string pathToFile; 
        private string headline = "Профессиональная химия для клининга";
        Dictionary<int, string> helplines = new Dictionary<int, string>();

        //листы для работы со словами 
        private List<Label> labelList = new List<Label>();
        private List<Label> headerList = new List<Label>();
        private List<Label> helpList = new List<Label>();

        //для координат
        //Dictionary<string, List<Dictionary<Point, Size>>> wordsCoords = new Dictionary<string, List<Dictionary<Point, Size>>>();

        //по идее должно быть настраиваемым параметром (оступ между словами)
        private static int wordMargin = 40;

        public NamingForm()
        {
            InitializeComponent();
        }

        private void NamingForm_Load(object sender, EventArgs e) //загружает слова из файла (нужно избавляться от констант)
        {
            pathToFile = @"d:\projects\screencapture\screencapture\bin\Debug\project\naming\ResearchObject\ResearchObject.txt";

            helplines.Add(0, "Кликните на слово, которое хотите исключить");

            this.WindowState = FormWindowState.Maximized;
            CreateHeadlineLabels();
            CreateLabelObjects(ReadFromFile(pathToFile));

            ShowRebuildTask("Сейчас вам предстоит выбрать из представленных на экране вариантов те, которые нужно исключить из рассмотрения.", "Продолжить");
        }
        
        private List<string> ReadFromFile(string pathToFile) //читает слова из файла
        {
            List<string> wordList = new List<string>();

            using (StreamReader sr = new StreamReader(pathToFile, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    wordList.Add(line);
                }
            }

            return wordList;
        }

        private void CreateHeadlineLabels() //создает хедеры
        {
            for (int i = 0; i < 2; i++)
            {
                CreateHeader(32, i);
            }
        }

        private void CreateHeader(int textSize, int position) //выводит хедеры на форму
        {
            Label header = new Label();
            header.Text = headline;
            header.Font = new Font("Arial", textSize);
            header.Width = this.ClientSize.Width;
            header.TextAlign = ContentAlignment.TopCenter;
            header.Visible = false;
            header.Size = new Size(header.Size.Width, Convert.ToInt32(textSize * 1.56));
            if (position < 1)
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.02));
            }  
            else
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - header.Size.Height));
            }

            CreateHelpLines(18, header.Location.Y, position);
            headerList.Add(header);
                
            this.Controls.Add(header);
        }

        private void setHelpLinesPosition(List<Label> headerList, List<Label> helpList) //корректирует позиции подсказок
        {
            foreach (Label currentWord in headerList)
            {
                currentWord.Width = this.ClientSize.Width;
            }
            foreach (Label currentWord in helpList)
            {
                currentWord.Width = this.ClientSize.Width;
            }

            try
            {
                headerList[headerList.Count - 1].Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - headerList[headerList.Count - 1].Size.Height));
                helpList[helpList.Count - 1].Location = new Point(0, headerList[headerList.Count - 1].Location.Y - helpList[helpList.Count - 1].Size.Height);
            }
            catch{}
        }

        private void CreateHelpLines(int textSize, int position, int mode) //создает и выводит на форму подсказки
        {
            Label help = new Label();
            help.Text = helplines[0]; //исправить 
            help.Font = new Font("Arial", textSize);
            help.Width = this.ClientSize.Width;
            help.TextAlign = ContentAlignment.TopCenter;
            help.Size = new Size(help.Size.Width, Convert.ToInt32(textSize * 1.56));
            help.ForeColor = Color.Gray;

            if(mode < 1)
                help.Location = new Point(0, position + help.Size.Height * 2);
            else
                help.Location = new Point(0, position - help.Size.Height);

            helpList.Add(help);
            this.Controls.Add(help);
        }

        private void CreateLabelObjects(List<string> wordList) //генерирует лейблы из принятых слов
        {
            foreach (string word in wordList)
            {
                labelList.Add(new Label());
                labelList[labelList.Count - 1].Text = word;
                labelList[labelList.Count - 1].AutoSize = true;
                labelList[labelList.Count - 1].Font = new Font("Arial", 32, FontStyle.Bold);
                labelList[labelList.Count - 1].Cursor = System.Windows.Forms.Cursors.Hand;
                labelList[labelList.Count - 1].Click += new System.EventHandler(RemoveLabelObject);
            }

            RandomLabelList(labelList);
        }

        private void HelpVisible(bool visible) //визибл подсказок
        {
            foreach (Label currentWord in helpList)
            {
                currentWord.Visible = visible;
            }
            foreach (Label currentWord in headerList)
            {
                currentWord.Visible = visible;
            }
        }

        private void RemoveLabelObject (object sender, EventArgs e) //событие при клике на кнопку, удаляющее лейбл и запускающее реролл 
        {
            if(labelList.Count > 1)
            {
                foreach (Label currentWord in labelList)
                {
                    if (sender == currentWord)
                    {
                        labelList.Remove(currentWord);
                        currentWord.Dispose();
                        break;
                    }
                }

                RandomLabelList(labelList);
            }
            else
            {
                ShowRebuildTask("Исследование завершено.\nВыполнить расчет и сохранить данные.", "ОК");
            }
        }

        private void ShowRebuildTask(string message, string button) //вызов окна с указанием для юзера
        {
            HelpVisible(false);

            foreach (Label currentWord in labelList)
            {
                currentWord.Visible = false;
            }

            descriptionLabel.Text = message;
            nextButton.Text = button;
            descPanel.Location = new Point((this.ClientSize.Width - descPanel.Size.Width) / 2, (this.ClientSize.Height - descPanel.Size.Height) / 2);
            descPanel.Visible = true;
        }

        private void SetPositionLabelObjects(List<Label> labelList) //расставляет слова по форме (нужно избавляться от констант)
        {
            int leftPadding = Convert.ToInt32(this.ClientSize.Width * 0.2);
            int topPadding = 100;

            int x = leftPadding;
            int y = topPadding;

            foreach (Label currentWord in labelList)
            {
                currentWord.Visible = false;
                currentWord.Location = new Point(x, y);
                this.Controls.Add(currentWord);
                if (x + wordMargin + leftPadding + currentWord.Size.Width * 2 < this.ClientSize.Width)
                    x = x + currentWord.Size.Width + wordMargin;
                else
                {
                    x = leftPadding;
                    y = y + currentWord.Size.Height + 10;
                }
            }

            CenterLabelAlignHorizontal(labelList);
            CenterLabelAlignVertical(labelList);
        }

        private void RandomLabelList(List<Label> labelList) //перемешает слова в листе
        {
            Random Rand = new Random();

            for (int i = 0; i < labelList.Count; i++)
            {
                Label tmp = labelList[0];
                labelList.RemoveAt(0);
                labelList.Insert(Rand.Next(labelList.Count), tmp);
            }

            SetPositionLabelObjects(labelList);
        }

        private void CenterLabelAlignHorizontal(List<Label> labelList) //центрует по горизонтали
        {
            if(labelList.Count > 0)
            {
                Label currentLine = labelList[0];
                int id = 0;

                foreach (Label currentWord in labelList)
                {
                    if (((id + 1) == labelList.Count) || (labelList[id + 1].Location.Y != currentLine.Location.Y))
                    {
                        int newLocation = (Convert.ToInt32(this.ClientSize.Width - ((labelList[id].Location.X + labelList[id].Size.Width) - currentLine.Location.X)) / 2) - currentLine.Location.X;
                        for (int i = labelList.IndexOf(currentLine); i <= id; i++)
                        {
                            labelList[i].Location = new Point(labelList[i].Location.X + newLocation, labelList[i].Location.Y);
                        }
                        if ((id + 1) != labelList.Count)
                            currentLine = labelList[id + 1];
                    }
                    id++;
                }
            }
        }

        private void CenterLabelAlignVertical(List<Label> labelList) //центрует по вертикали
        {
            try
            {
                int sizeHeigh = labelList[labelList.Count - 1].Location.Y - labelList[0].Location.Y + labelList[labelList.Count - 1].Size.Height;

                if (sizeHeigh >= this.ClientSize.Height * 0.75)
                {
                    SizingLabelText(labelList, Convert.ToInt32(labelList[0].Font.Size) - 2);
                }
                else
                {
                    if ((sizeHeigh < this.ClientSize.Height * 0.5) & !(sizeHeigh > this.ClientSize.Height * 0.75) & (labelList[0].Font.Size < 40))
                    {
                        SizingLabelText(labelList, Convert.ToInt32(labelList[0].Font.Size) + 2);
                    }
                    else
                    {
                        int diffHeigh = Convert.ToInt32((this.ClientSize.Height - sizeHeigh) / 2) - labelList[0].Location.Y;

                        foreach (Label currentWord in labelList)
                        {
                            currentWord.Location = new Point(currentWord.Location.X, currentWord.Location.Y + diffHeigh);
                            currentWord.Visible = true;
                        }
                        //WriteCoordinats(labelList);
                    }
                }
            }
            catch { }
        }

        //private void WriteCoordinats(List<Label> labelList)
        //{
        //    foreach (Label currentWord in labelList)
        //    {
        //        if (wordsCoords.ContainsKey(currentWord.Text))
        //        {
        //            //Point newCoords = ;
        //            //wordsCoords[currentWord.Text] = wordsCoords[currentWord.Text].Add(currentWord.Location);
        //        }
        //        else
        //        {
        //            List<string> newCoords = new List<string>();
        //            newCoords.Add(currentWord.Location.ToString() + currentWord.Size.ToString());
        //            wordsCoords.Add(currentWord.Text, newCoords);
        //        }
        //        //wordsCoords.Add(currentWord.Text, );
        //    }
        //}

        private void SizingLabelText(List<Label> labelList, int size) //устанавливает размер шрифтов слов
        {
            if (labelList.Count > 1)
            {
                foreach (Label currentWord in labelList)
                {
                    currentWord.Font = new Font("Arial", size, FontStyle.Bold);
                }
                SetPositionLabelObjects(labelList);
            }
            else
            {
                labelList[0].Location = new Point((this.ClientSize.Width - labelList[0].Size.Width)/2, (this.ClientSize.Height - labelList[0].Size.Height) / 2);
                labelList[0].Visible = true;
            }
        }

        private void NamingForm_Resize(object sender, EventArgs e) //устанавливает позицию для слов и подсказок при резайзе формы
        {
            SetPositionLabelObjects(labelList);
            setHelpLinesPosition(headerList, helpList);
        }

        private void nextButton_Click(object sender, EventArgs e) //продолжать процесс исследования после смены задания
        {
            if (nextButton.Text == "ОК")
            {
                this.Close();
            }
            else
            {
                foreach (Label currentWord in labelList)
                {
                    currentWord.Visible = true;
                }

                descPanel.Visible = false;
                HelpVisible(true);
            }
        }
    }
}
