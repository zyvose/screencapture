﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class BlackBoxStimulator : Form
    {
        SerialPort port = new SerialPort();
        string portName;
        string commandLine = "";


        int blackBoxesCount;
        int currentBlackBoxNumber;
        private string headline = "Стимулятор черного ящика";
        Dictionary<int, string> helplines = new Dictionary<int, string>();
        private List<Label> headerList = new List<Label>();
        private List<Label> helpList = new List<Label>();
        bool isParallel;
        int showItemTime;
        public BlackBoxStimulator(int blackBoxesCount, int showItemTime, bool isParallel, string portName)//если isParallel=true, то все ящики должны открываться одновременно, иначе - ящики открываются последовательно
        {
            this.portName = portName;
            this.showItemTime = showItemTime*1000;
            this.blackBoxesCount = blackBoxesCount;
            this.isParallel = isParallel;
            InitializeComponent();
            
        }
        private void BlackBoxStimulator_Load(object sender, EventArgs e)
        {



            



            helplines.Add(0, "Осматривайте то, что будет в черном ящике");

            this.WindowState = FormWindowState.Maximized;
            CreateHeadlineLabels();
            showTimer.Interval = showItemTime;

            ShowRebuildTask("Сейчас вам предстоит осмотреть представленные предметы", "Начать исследование");
        }

        private void CreateHeadlineLabels() //создает хедеры
        {
            for (int i = 0; i < 2; i++)
            {
                CreateHeader(32, i);
            }
        }

        private void CreateHeader(int textSize, int position) //выводит хедеры на форму
        {
            Label header = new Label();
            header.Text = headline;
            header.Font = new Font("Arial", textSize);
            header.Width = this.ClientSize.Width;
            header.TextAlign = ContentAlignment.TopCenter;
            header.Visible = false;
            header.Size = new Size(header.Size.Width, Convert.ToInt32(textSize * 1.56));
            if (position < 1)
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.02));
            }
            else
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - header.Size.Height));
            }

            CreateHelpLines(18, header.Location.Y, position);
            headerList.Add(header);

            this.Controls.Add(header);
        }

        private void CreateHelpLines(int textSize, int position, int mode) //создает и выводит на форму подсказки
        {


            Label help = new Label();
            help.Text = helplines[0]; //исправить 
            help.Font = new Font("Arial", textSize);
            help.Width = this.ClientSize.Width;
            help.TextAlign = ContentAlignment.TopCenter;
            help.Size = new Size(help.Size.Width, Convert.ToInt32(textSize * 1.56));
            help.ForeColor = Color.Gray;

            if (mode < 1)
                help.Location = new Point(0, position + help.Size.Height * 2);
            else
                help.Location = new Point(0, position - help.Size.Height);

            helpList.Add(help);
            this.Controls.Add(help);
        }

        private void setHelpLinesPosition(List<Label> headerList, List<Label> helpList) //корректирует позиции подсказок
        {
            foreach (Label currentWord in headerList)
            {
                currentWord.Width = this.ClientSize.Width;
            }
            foreach (Label currentWord in helpList)
            {
                currentWord.Width = this.ClientSize.Width;
            }

            try
            {
                headerList[headerList.Count - 1].Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - headerList[headerList.Count - 1].Size.Height));
                helpList[helpList.Count - 1].Location = new Point(0, headerList[headerList.Count - 1].Location.Y - helpList[helpList.Count - 1].Size.Height);
            }
            catch { }
        }

        private void HelpVisible(bool visible) //визибл подсказок
        {
            foreach (Label currentWord in helpList)
            {
                currentWord.Visible = visible;
            }
            foreach (Label currentWord in headerList)
            {
                currentWord.Visible = visible;
            }
        }

        private void ShowRebuildTask(string message, string button) //вызов окна с указанием для юзера
        {
            HelpVisible(false);

            descriptionLabel.Text = message;
            nextButton.Text = button;
            descPanel.Location = new Point((this.ClientSize.Width - descPanel.Size.Width) / 2, (this.ClientSize.Height - descPanel.Size.Height) / 2);
            descPanel.Visible = true;
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            tipLabel.Visible = false;
            
            if (isParallel)
            {
                if (nextButton.Text == "OK")
                {
                    CloseAll();
                    this.Close();
                }
                else
                {
                    OpenAll();
                }
           
            }
            else
            {
                if (nextButton.Text == "OK")
                {
                    CloseBlackBox(currentBlackBoxNumber);
                    this.Close();
                }
                else
                {
                    if (nextButton.Text == "Продолжить")
                    {
                        CloseBlackBox(currentBlackBoxNumber);
                        currentBlackBoxNumber++;
                    }
                    OpenBlackBox(currentBlackBoxNumber);
                    descPanel.Visible = false;
                    HelpVisible(true);
                    showTimer.Start();
                }
            }
        }

        private void OpenBlackBox(int number)//Здесь должен быть код открытия черного ящика
        {
            try
            {
                commandLine = "8" + number.ToString();
                sendCom(commandLine);

            }
            catch { }
            richTextBox1.Text += "\nOpen blackbox " + number;//только для тестирования
        }

        private void CloseBlackBox(int number)//Здесь должен быть код закрытия черного ящика
        {
            try
            {
                commandLine = "9" + number.ToString();
                sendCom(commandLine);

            }
            catch { }
            richTextBox1.Text += "\nClose blackbox " + number;//только для тестирования
        }
        private void FinishStimulation()
        {
            CloseBlackBox(currentBlackBoxNumber);
            ShowRebuildTask("Исследование завершено", "OK");
        }
        private void OpenAll()
        {
            try
            {
                commandLine = "70";
                sendCom(commandLine);

            }
            catch { }
            richTextBox1.Text += "\nOpen all blackboxes ";//только для тестирования
        }
        private void CloseAll()
        {
            try
            {
                commandLine = "76";
                sendCom(commandLine);

            }
            catch { }
            richTextBox1.Text += "\nClose all blackboxes ";//только для тестирования
        }
        private void BlackBoxStimulator_Resize(object sender, EventArgs e)
        {
            
            setHelpLinesPosition(headerList, helpList);
        }

        private void showTimer_Tick(object sender, EventArgs e)
        {
            showTimer.Stop();
            if (isParallel)
            { ShowRebuildTask("Исследование завершено\nПожалуйста, верните предметы в ящики и нажмите \"OK\"", "OK"); }
            else
            {
                if (currentBlackBoxNumber < blackBoxesCount-1)
                { ShowRebuildTask("Пожалуйста, верните предмет в ящик и нажмите \"Продолжить\"", "Продолжить"); }
                    else
                {
                    ShowRebuildTask("Исследование завершено\nПожалуйста, верните предмет в ящик и нажмите \"OK\"", "OK");
                }
            }
        }

        

        private void sendCom(string commandLine)//отправка команды по COM-порту
        {
            
            port.BaudRate = 9600;
            port.PortName = portName;
            port.Open();
            port.WriteLine(commandLine);
            port.Close();
        }
    }
}
