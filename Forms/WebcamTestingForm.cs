﻿using Accord.Video.FFMPEG;
using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class WebcamTestingForm : Form
    {
       
        private FilterInfoCollection videoDevices;

        private VideoCaptureDevice FinalVideo = null;
        private VideoCaptureDeviceForm captureDevice;
        private Bitmap video;
        //private AVIWriter AVIwriter = new AVIWriter();
        private VideoFileWriter FileWriter = new VideoFileWriter();
        private SaveFileDialog saveAvi;
        public WebcamTestingForm()
        {
            InitializeComponent();
        }

        private void WebcamTestingForm_Load(object sender, EventArgs e)
        {
            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            
            captureDevice = new VideoCaptureDeviceForm();
            
        }

        void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            
                video = (Bitmap)eventArgs.Frame.Clone();
                pictureBox1.Image = (Bitmap)eventArgs.Frame.Clone();
                FileWriter.WriteVideoFrame(video);
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            if (captureDevice.ShowDialog(this) == DialogResult.OK)
            {
                FinalVideo = captureDevice.VideoDevice;
                FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
                
                startButton.Enabled = true;
            }
        }    
        private void startButton_Click(object sender, EventArgs e)
        { 
            saveAvi = new SaveFileDialog();
            saveAvi.Filter = "Avi Files (*.avi)|*.avi";
            if (saveAvi.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int h = captureDevice.VideoDevice.VideoResolution.FrameSize.Height;
                int w = captureDevice.VideoDevice.VideoResolution.FrameSize.Width;
                FileWriter.Open(saveAvi.FileName, w, h, 25, VideoCodec.Default, 5000000);
                
                FinalVideo.Start();
               
               
               
                settingsButton.Enabled = false;
                startButton.Enabled = false;
                stopButton.Enabled = true;

            }
                
                
                
            

        }

        
        private void stopButton_Click(object sender, EventArgs e)
        {
            this.FinalVideo.Stop();
            FileWriter.Close();
            pictureBox1.Image = null;
            settingsButton.Enabled = true;
            stopButton.Enabled = false;
            startButton.Enabled = true;
            

        }

        
    }
}
