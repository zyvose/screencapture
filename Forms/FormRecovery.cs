﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace screencapture.Forms
{
    public partial class FormRecovery : Form
    {
        private PointDataSQLite SQLite;

        bool readyToCreate;

        public FormRecovery()
        {
            InitializeComponent();
        }

        private void FormRecovery_Load(object sender, EventArgs e)
        {
            readyToCreate = false;

            if (ConfigurationManager.AppSettings.AllKeys.Contains("Default_Project_Directory"))
            {
                ProjectDirectoryTextBox.Text = ConfigurationManager.AppSettings["Default_Project_Directory"];
                projectDirSearch(ConfigurationManager.AppSettings["Default_Project_Directory"]);
            }
                
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

            DialogResult result = folderBrowser.ShowDialog();
        
            if (!string.IsNullOrWhiteSpace(folderBrowser.SelectedPath))
            {
                ProjectDirectoryTextBox.Text = folderBrowser.SelectedPath;
                projectDirSearch(folderBrowser.SelectedPath);
            }
        }

        private void projectDirSearch(string projectDir)
        {
            projectList.Items.Clear();

            string[] directories = Directory.GetDirectories(projectDir);
            if (directories.Length > 0)
            {
                foreach (var dir in directories)
                {
                    if (ProjectInit(dir)){
                        int i = dir.LastIndexOf(@"\") + 1;
                        string nameProject = dir.Substring(i);
                        projectList.Items.Add(nameProject);
                    }
                }
                if(projectList.Items.Count == 0)
                    MessageBox.Show("Корретных проектов не обнаружено!", "Eye Stream Capture");
            }
            else
            {
                ProjectDirectoryTextBox.Text = string.Empty;
                MessageBox.Show("В это директории проекты не найдены!", "Eye Stream Capture");
            }
        }

        private bool ProjectInit(string projectPath) //определяет, является ли папка проектом
        {
            DirectoryInfo dir = new DirectoryInfo(projectPath);
            if (Directory.GetFiles(projectPath, dir.Name + ".sqlite").Length > 0)
                return true;
            else
                return false;
        }

        private void ProjectExamSearch(string projectPath)
        {
            string[] videoPaths = Directory.GetFiles(projectPath, "*.avi");
            if (videoPaths != null)
            {
                foreach (string path in videoPaths)
                {
                    string examName = path.Replace(projectPath, string.Empty);

                    examName = examName.Replace(".avi", string.Empty);
                    examName = examName.Replace("\\", string.Empty);
                    examineeList.Items.Add(examName);
                }
                examineeList.SelectedIndex = 0;
            }
        }

        private void projectList_SelectedValueChanged(object sender, EventArgs e)
        {
            readyToCreate = true;

            ProjectExamSearch(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString());

            //temp файл с логом таймштампа
            if (Directory.GetFiles(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString(), "temp_timestamp.txt").Count() > 0)
            {
                timeBox.Checked = true;
                timeBox.Enabled = false;
            }
            else
            {
                timeBox.Checked = false;
                timeBox.Enabled = true;
                readyToCreate = false;
            }

            //temp файл с логом координат мышки
            if (Directory.GetFiles(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString(), "temp_mouse.txt").Count() > 0)
            {
                mouseBox.Checked = true;
                mouseBox.Enabled = false;
            }
            else
            {
                mouseBox.Checked = false;
                mouseBox.Enabled = true;
                readyToCreate = false;
            }

            //temp файл с логом шапки
            if (Directory.GetFiles(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString(), "temp_mind.txt").Count() > 0)
            {
                mindBox.Checked = true;
                mindBox.Enabled = false;
            }
            else
            {
                mindBox.Checked = false;
                mindBox.Enabled = true;
                readyToCreate = false;
            }

            //temp файл с логом глаз
            if (Directory.GetFiles(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString(), "temp_eye.txt").Count() > 0)
            {
                eyeBox.Checked = true;
                eyeBox.Enabled = false;
            }
            else
            {
                eyeBox.Checked = false;
                eyeBox.Enabled = true;
                readyToCreate = false;
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (readyToCreate)
            {
                //SQLite.LogWriting(); - основной метод наполнения через лист

                List<List<string>> templist = new List<List<string>>();

                List<string> timestampList = fileToList(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString() + @"\" + "temp_timestamp.txt");
                templist.Add(timestampList);

                List<string> mouseList = fileToList(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString() + @"\" + "temp_mouse.txt");
                templist.Add(mouseList);

                List<string> mindList = fileToList(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString() + @"\" + "temp_mind.txt");
                templist.Add(mindList);

                List<string> eyeList = fileToList(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString() + @"\" + "temp_eye.txt");
                templist.Add(eyeList);

                //Создание/открытие БД и таблица с логом
                SQLite = new PointDataSQLite();
                SQLite.ListPointsData = new List<FramePoint>(); //инциализирую лист
                SQLite.CreateOrUsingDB(ProjectDirectoryTextBox.Text + @"\" + projectList.SelectedItem.ToString() + @"\" + projectList.SelectedItem.ToString() + ".sqlite", examineeList.SelectedItem.ToString());

                for (var i = 0; i < shortList(templist); i++) {
                    string[] eye = eyeList[i].Split(' ');
                    string[] mouse = mouseList[i].Split(' ');
                    string[] mind = mindList[i].Split(' ');
                    
                  //  SQLite.LogWriting(double.Parse(eye[0]), double.Parse(eye[1]), double.Parse(mouse[0]), double.Parse(mouse[1]), double.Parse(timestampList[i]), 0, 0, Int32.Parse(mind[0]), Int32.Parse(mind[1]));
                }

                SQLite.SerializeToBD();
            }
            else
            {
                MessageBox.Show("Проект не выбран или не готов к сборке!", "Eye Stream Capture");
            }
        }

        private List<string> fileToList(string pathToFile)
        {
            List<string> valueList = new List<string>();

            using (StreamReader sr = new StreamReader(pathToFile, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    valueList.Add(line);
                }
            }

            return valueList;
        }

        private int shortList(List<List<string>> templist)
        {
            int minCount = templist[0].Count();

            foreach (List<string> valuelist in templist)
            {
                if (valuelist.Count() < minCount)
                    minCount = valuelist.Count();
            }

            return minCount;
        }

    }
}
