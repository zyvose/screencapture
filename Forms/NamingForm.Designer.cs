﻿namespace screencapture.Forms
{
    partial class NamingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.descPanel = new System.Windows.Forms.Panel();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.nextButton = new System.Windows.Forms.Button();
            this.descPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // descPanel
            // 
            this.descPanel.BackColor = System.Drawing.Color.Transparent;
            this.descPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.descPanel.Controls.Add(this.nextButton);
            this.descPanel.Controls.Add(this.descriptionLabel);
            this.descPanel.Location = new System.Drawing.Point(68, 54);
            this.descPanel.Name = "descPanel";
            this.descPanel.Size = new System.Drawing.Size(1000, 400);
            this.descPanel.TabIndex = 0;
            this.descPanel.Visible = false;
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.descriptionLabel.Location = new System.Drawing.Point(74, 45);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(852, 271);
            this.descriptionLabel.TabIndex = 0;
            this.descriptionLabel.Text = "Сейчас вам предстоит выбрать из представленных на экране вариантов наилучший";
            this.descriptionLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // nextButton
            // 
            this.nextButton.AutoSize = true;
            this.nextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextButton.Location = new System.Drawing.Point(431, 334);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(150, 36);
            this.nextButton.TabIndex = 1;
            this.nextButton.Text = "Продолжить";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // NamingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1207, 542);
            this.Controls.Add(this.descPanel);
            this.Name = "NamingForm";
            this.Text = "NamingForm";
            this.Load += new System.EventHandler(this.NamingForm_Load);
            this.Resize += new System.EventHandler(this.NamingForm_Resize);
            this.descPanel.ResumeLayout(false);
            this.descPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel descPanel;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Button nextButton;
    }
}