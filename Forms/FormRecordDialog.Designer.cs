﻿namespace screencapture
{
    partial class FormRecordDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartRec = new System.Windows.Forms.Button();
            this.btnCancelRD = new System.Windows.Forms.Button();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbDescription = new System.Windows.Forms.RichTextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.cbOpenResearchObject = new System.Windows.Forms.CheckBox();
            this.statusLabel = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.pictureSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.delayNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.showTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FIOtextBox = new System.Windows.Forms.TextBox();
            this.settingsGroupBox = new System.Windows.Forms.GroupBox();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.videoSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.videoDelayNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.videoTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.soundSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.delaySoundNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.soundTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.loadBox = new System.Windows.Forms.PictureBox();
            this.browserButton = new System.Windows.Forms.Button();
            this.buttonExperimentalStimulator = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pictureSettingsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showTimeNumericUpDown)).BeginInit();
            this.settingsGroupBox.SuspendLayout();
            this.videoSettingsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoDelayNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoTimeNumericUpDown)).BeginInit();
            this.soundSettingsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delaySoundNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundTimeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadBox)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStartRec
            // 
            this.btnStartRec.Location = new System.Drawing.Point(90, 511);
            this.btnStartRec.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartRec.Name = "btnStartRec";
            this.btnStartRec.Size = new System.Drawing.Size(157, 43);
            this.btnStartRec.TabIndex = 0;
            this.btnStartRec.Text = "Запись";
            this.btnStartRec.UseVisualStyleBackColor = true;
            this.btnStartRec.Click += new System.EventHandler(this.btnStartRec_Click);
            // 
            // btnCancelRD
            // 
            this.btnCancelRD.Location = new System.Drawing.Point(272, 511);
            this.btnCancelRD.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelRD.Name = "btnCancelRD";
            this.btnCancelRD.Size = new System.Drawing.Size(153, 43);
            this.btnCancelRD.TabIndex = 1;
            this.btnCancelRD.Text = "Отмена";
            this.btnCancelRD.UseVisualStyleBackColor = true;
            this.btnCancelRD.Click += new System.EventHandler(this.btnCancelRD_Click);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(43, 46);
            this.tbName.Margin = new System.Windows.Forms.Padding(4);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(441, 22);
            this.tbName.TabIndex = 2;
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(43, 349);
            this.tbDescription.Margin = new System.Windows.Forms.Padding(4);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(441, 117);
            this.tbDescription.TabIndex = 3;
            this.tbDescription.Text = "";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName.Location = new System.Drawing.Point(37, 17);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(171, 25);
            this.labelName.TabIndex = 4;
            this.labelName.Text = "Имя \\ Название";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDescription.Location = new System.Drawing.Point(205, 320);
            this.labelDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(110, 25);
            this.labelDescription.TabIndex = 5;
            this.labelDescription.Text = "Описание";
            // 
            // cbOpenResearchObject
            // 
            this.cbOpenResearchObject.AutoSize = true;
            this.cbOpenResearchObject.Checked = true;
            this.cbOpenResearchObject.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbOpenResearchObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbOpenResearchObject.Location = new System.Drawing.Point(90, 474);
            this.cbOpenResearchObject.Margin = new System.Windows.Forms.Padding(4);
            this.cbOpenResearchObject.Name = "cbOpenResearchObject";
            this.cbOpenResearchObject.Size = new System.Drawing.Size(353, 29);
            this.cbOpenResearchObject.TabIndex = 6;
            this.cbOpenResearchObject.Text = "Показывать объект исследования";
            this.cbOpenResearchObject.UseVisualStyleBackColor = true;
            // 
            // statusLabel
            // 
            this.statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusLabel.Location = new System.Drawing.Point(87, 558);
            this.statusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(397, 28);
            this.statusLabel.TabIndex = 7;
            this.statusLabel.Text = "Подождите, идет подготовка оборудования...";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.statusLabel.Visible = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(9, 75);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(187, 21);
            this.radioButton1.TabIndex = 9;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Обычное исследование";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(211, 75);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(229, 21);
            this.radioButton2.TabIndex = 10;
            this.radioButton2.Text = "Стимуляция с изображениями";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // pictureSettingsGroupBox
            // 
            this.pictureSettingsGroupBox.Controls.Add(this.delayNumericUpDown);
            this.pictureSettingsGroupBox.Controls.Add(this.label3);
            this.pictureSettingsGroupBox.Controls.Add(this.showTimeNumericUpDown);
            this.pictureSettingsGroupBox.Controls.Add(this.label2);
            this.pictureSettingsGroupBox.Enabled = false;
            this.pictureSettingsGroupBox.Location = new System.Drawing.Point(211, 102);
            this.pictureSettingsGroupBox.Name = "pictureSettingsGroupBox";
            this.pictureSettingsGroupBox.Size = new System.Drawing.Size(302, 125);
            this.pictureSettingsGroupBox.TabIndex = 11;
            this.pictureSettingsGroupBox.TabStop = false;
            this.pictureSettingsGroupBox.Text = "Настройки стимуляции с изображениями";
            // 
            // delayNumericUpDown
            // 
            this.delayNumericUpDown.Location = new System.Drawing.Point(9, 88);
            this.delayNumericUpDown.Name = "delayNumericUpDown";
            this.delayNumericUpDown.Size = new System.Drawing.Size(47, 22);
            this.delayNumericUpDown.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(217, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Пауза между изображениями, с";
            // 
            // showTimeNumericUpDown
            // 
            this.showTimeNumericUpDown.Location = new System.Drawing.Point(9, 38);
            this.showTimeNumericUpDown.Name = "showTimeNumericUpDown";
            this.showTimeNumericUpDown.Size = new System.Drawing.Size(47, 22);
            this.showTimeNumericUpDown.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Время показа изображения, с";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "ФИО испытуемого";
            // 
            // FIOtextBox
            // 
            this.FIOtextBox.Location = new System.Drawing.Point(9, 47);
            this.FIOtextBox.Name = "FIOtextBox";
            this.FIOtextBox.Size = new System.Drawing.Size(293, 22);
            this.FIOtextBox.TabIndex = 1;
            // 
            // settingsGroupBox
            // 
            this.settingsGroupBox.Controls.Add(this.radioButton7);
            this.settingsGroupBox.Controls.Add(this.radioButton6);
            this.settingsGroupBox.Controls.Add(this.videoSettingsGroupBox);
            this.settingsGroupBox.Controls.Add(this.soundSettingsGroupBox);
            this.settingsGroupBox.Controls.Add(this.radioButton5);
            this.settingsGroupBox.Controls.Add(this.radioButton4);
            this.settingsGroupBox.Controls.Add(this.radioButton3);
            this.settingsGroupBox.Controls.Add(this.radioButton1);
            this.settingsGroupBox.Controls.Add(this.pictureSettingsGroupBox);
            this.settingsGroupBox.Controls.Add(this.radioButton2);
            this.settingsGroupBox.Controls.Add(this.label1);
            this.settingsGroupBox.Controls.Add(this.FIOtextBox);
            this.settingsGroupBox.Location = new System.Drawing.Point(43, 85);
            this.settingsGroupBox.Name = "settingsGroupBox";
            this.settingsGroupBox.Size = new System.Drawing.Size(1225, 232);
            this.settingsGroupBox.TabIndex = 12;
            this.settingsGroupBox.TabStop = false;
            this.settingsGroupBox.Text = "Настройки исследования";
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(932, 21);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(135, 21);
            this.radioButton7.TabIndex = 22;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "тест вебкамеры";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(523, 27);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(211, 21);
            this.radioButton6.TabIndex = 21;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Стимулятор черного ящика";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // videoSettingsGroupBox
            // 
            this.videoSettingsGroupBox.Controls.Add(this.videoDelayNumericUpDown);
            this.videoSettingsGroupBox.Controls.Add(this.videoTimeNumericUpDown);
            this.videoSettingsGroupBox.Controls.Add(this.label7);
            this.videoSettingsGroupBox.Controls.Add(this.label6);
            this.videoSettingsGroupBox.Enabled = false;
            this.videoSettingsGroupBox.Location = new System.Drawing.Point(523, 105);
            this.videoSettingsGroupBox.Name = "videoSettingsGroupBox";
            this.videoSettingsGroupBox.Size = new System.Drawing.Size(227, 121);
            this.videoSettingsGroupBox.TabIndex = 20;
            this.videoSettingsGroupBox.TabStop = false;
            this.videoSettingsGroupBox.Text = "Настройки видеостимулятора";
            // 
            // videoDelayNumericUpDown
            // 
            this.videoDelayNumericUpDown.Location = new System.Drawing.Point(10, 85);
            this.videoDelayNumericUpDown.Name = "videoDelayNumericUpDown";
            this.videoDelayNumericUpDown.Size = new System.Drawing.Size(43, 22);
            this.videoDelayNumericUpDown.TabIndex = 3;
            // 
            // videoTimeNumericUpDown
            // 
            this.videoTimeNumericUpDown.Location = new System.Drawing.Point(10, 43);
            this.videoTimeNumericUpDown.Name = "videoTimeNumericUpDown";
            this.videoTimeNumericUpDown.Size = new System.Drawing.Size(43, 22);
            this.videoTimeNumericUpDown.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Пауза между видео, с";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Время показа видео, с";
            // 
            // soundSettingsGroupBox
            // 
            this.soundSettingsGroupBox.Controls.Add(this.label4);
            this.soundSettingsGroupBox.Controls.Add(this.delaySoundNumericUpDown);
            this.soundSettingsGroupBox.Controls.Add(this.label5);
            this.soundSettingsGroupBox.Controls.Add(this.soundTimeNumericUpDown);
            this.soundSettingsGroupBox.Enabled = false;
            this.soundSettingsGroupBox.Location = new System.Drawing.Point(923, 105);
            this.soundSettingsGroupBox.Name = "soundSettingsGroupBox";
            this.soundSettingsGroupBox.Size = new System.Drawing.Size(258, 129);
            this.soundSettingsGroupBox.TabIndex = 19;
            this.soundSettingsGroupBox.TabStop = false;
            this.soundSettingsGroupBox.Text = "Настройки звукового стимулятора";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(205, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Время прслушивания звука, с";
            // 
            // delaySoundNumericUpDown
            // 
            this.delaySoundNumericUpDown.Location = new System.Drawing.Point(9, 83);
            this.delaySoundNumericUpDown.Name = "delaySoundNumericUpDown";
            this.delaySoundNumericUpDown.Size = new System.Drawing.Size(45, 22);
            this.delaySoundNumericUpDown.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(165, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Пауза между звуками, с";
            // 
            // soundTimeNumericUpDown
            // 
            this.soundTimeNumericUpDown.Location = new System.Drawing.Point(9, 38);
            this.soundTimeNumericUpDown.Name = "soundTimeNumericUpDown";
            this.soundTimeNumericUpDown.Size = new System.Drawing.Size(45, 22);
            this.soundTimeNumericUpDown.TabIndex = 15;
            // 
            // radioButton5
            // 
            this.radioButton5.Location = new System.Drawing.Point(923, 75);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(179, 24);
            this.radioButton5.TabIndex = 14;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Звуковой стимулятор";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(693, 75);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(223, 21);
            this.radioButton4.TabIndex = 13;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Изображения с исключением";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(523, 75);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(163, 21);
            this.radioButton3.TabIndex = 12;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Стимуляция с видео";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // loadBox
            // 
            this.loadBox.Image = global::screencapture.Properties.Resources.ajax_loader;
            this.loadBox.Location = new System.Drawing.Point(242, 590);
            this.loadBox.Margin = new System.Windows.Forms.Padding(4);
            this.loadBox.Name = "loadBox";
            this.loadBox.Size = new System.Drawing.Size(43, 11);
            this.loadBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.loadBox.TabIndex = 8;
            this.loadBox.TabStop = false;
            this.loadBox.Visible = false;
            // 
            // browserButton
            // 
            this.browserButton.Location = new System.Drawing.Point(843, 474);
            this.browserButton.Name = "browserButton";
            this.browserButton.Size = new System.Drawing.Size(116, 53);
            this.browserButton.TabIndex = 13;
            this.browserButton.Text = "Тест формы с браузером";
            this.browserButton.UseVisualStyleBackColor = true;
            this.browserButton.Click += new System.EventHandler(this.browserButton_Click);
            // 
            // buttonExperimentalStimulator
            // 
            this.buttonExperimentalStimulator.Location = new System.Drawing.Point(1019, 461);
            this.buttonExperimentalStimulator.Name = "buttonExperimentalStimulator";
            this.buttonExperimentalStimulator.Size = new System.Drawing.Size(160, 66);
            this.buttonExperimentalStimulator.TabIndex = 14;
            this.buttonExperimentalStimulator.Text = "Тест экспериментального стимулятора";
            this.buttonExperimentalStimulator.UseVisualStyleBackColor = true;
            this.buttonExperimentalStimulator.Click += new System.EventHandler(this.buttonExperimentalStimulator_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(700, 468);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 65);
            this.button1.TabIndex = 15;
            this.button1.Text = "Тест стимулятора черного ящика";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(700, 423);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(123, 24);
            this.comboBox1.TabIndex = 16;
            // 
            // FormRecordDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 608);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonExperimentalStimulator);
            this.Controls.Add(this.browserButton);
            this.Controls.Add(this.settingsGroupBox);
            this.Controls.Add(this.loadBox);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.cbOpenResearchObject);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.btnCancelRD);
            this.Controls.Add(this.btnStartRec);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormRecordDialog";
            this.Text = "FormRecordDialog";
            this.Load += new System.EventHandler(this.FormRecordDialog_Load);
            this.pictureSettingsGroupBox.ResumeLayout(false);
            this.pictureSettingsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showTimeNumericUpDown)).EndInit();
            this.settingsGroupBox.ResumeLayout(false);
            this.settingsGroupBox.PerformLayout();
            this.videoSettingsGroupBox.ResumeLayout(false);
            this.videoSettingsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoDelayNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoTimeNumericUpDown)).EndInit();
            this.soundSettingsGroupBox.ResumeLayout(false);
            this.soundSettingsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delaySoundNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundTimeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartRec;
        private System.Windows.Forms.Button btnCancelRD;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.RichTextBox tbDescription;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.CheckBox cbOpenResearchObject;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.PictureBox loadBox;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.GroupBox pictureSettingsGroupBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FIOtextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown delayNumericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown showTimeNumericUpDown;
        private System.Windows.Forms.GroupBox settingsGroupBox;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown delaySoundNumericUpDown;
        private System.Windows.Forms.NumericUpDown soundTimeNumericUpDown;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.GroupBox soundSettingsGroupBox;
        private System.Windows.Forms.GroupBox videoSettingsGroupBox;
        private System.Windows.Forms.NumericUpDown videoDelayNumericUpDown;
        private System.Windows.Forms.NumericUpDown videoTimeNumericUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.Button browserButton;
        private System.Windows.Forms.Button buttonExperimentalStimulator;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}