﻿namespace screencapture.Forms
{
    partial class PictureExclusionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.descPanel = new System.Windows.Forms.Panel();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.nextButton = new System.Windows.Forms.Button();
            this.descPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // descPanel
            // 
            this.descPanel.Controls.Add(this.nextButton);
            this.descPanel.Controls.Add(this.descriptionLabel);
            this.descPanel.Location = new System.Drawing.Point(254, 65);
            this.descPanel.Name = "descPanel";
            this.descPanel.Size = new System.Drawing.Size(1036, 481);
            this.descPanel.TabIndex = 1;
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLabel.Location = new System.Drawing.Point(47, 24);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(960, 299);
            this.descriptionLabel.TabIndex = 0;
            this.descriptionLabel.Text = "Сейчас вам предстоит выбрать из представленных на экране вариантов наилучший";
            this.descriptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nextButton
            // 
            this.nextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextButton.Location = new System.Drawing.Point(381, 400);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(272, 66);
            this.nextButton.TabIndex = 1;
            this.nextButton.Text = "Продолжить";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // PictureExclusionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1572, 587);
            this.Controls.Add(this.descPanel);
            this.Name = "PictureExclusionForm";
            this.Text = "PictureExclusionForm";
            this.Load += new System.EventHandler(this.PictureExclusionForm_Load);
            this.descPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel descPanel;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Label descriptionLabel;
    }
}