﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class SelectPictureForm : Form
    {
        Bitmap picture;
        string finalPicsDiretory = "D:\\Брендмашина\\3\\";
        string image;
        string request;
        string testerName;
        string emotion;
        public SelectPictureForm(string image, string request, string testerName, string emotion)
        {
            this.request = request;
            this.testerName = testerName;
            this.emotion = emotion;
            this.image = image;
            InitializeComponent();
            
        }

        private void SelectPictureForm_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            selectedPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            using (var fs = File.Open(image, FileMode.Open))
                picture = new Bitmap(fs);
            selectedPictureBox.Image = picture;



        }
        private void setPictureBoxPosition()
        {
            int leftPadding = Convert.ToInt32(ClientSize.Width * 0.1);
            int topPadding = 100;
            selectedPictureBox.Height = ClientSize.Height - 2*topPadding;
            selectedPictureBox.Width = ClientSize.Width -2*leftPadding;
            selectedPictureBox.Location = new Point(leftPadding, topPadding);

        }
        private void setButtonsPosition()
        {
            buttonYes.Location = new Point(Convert.ToInt32(ClientSize.Width * 0.33-buttonYes.Width/2), ClientSize.Height - 70);
            buttonNo.Location = new Point(Convert.ToInt32(ClientSize.Width * 0.66-buttonNo.Width/2), ClientSize.Height - 70);
        }
    
        private void SelectPictureForm_Resize(object sender, EventArgs e)
        {
            setPictureBoxPosition();
            setButtonsPosition();
            descriptionLabel.Location = new Point((ClientSize.Width - descriptionLabel.Width) / 2, 50 - descriptionLabel.Height / 2);
            Focus();
            
        }


        private void buttonYes_Click(object sender, EventArgs e)
        {
            try
            {
                File.Delete(finalPicsDiretory + request + "_" + emotion + "_" + testerName + ".jpg");
            }
            catch
            {

            }
            try
            {
                File.Copy(image, finalPicsDiretory + request + "_" + emotion + "_" + testerName + ".jpg");
                MessageBox.Show("Файл сохранен");
            }
            catch
            { MessageBox.Show("Ошибка при сохранении файла"); }
            
            Close();
        }

        private void buttonNo_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
