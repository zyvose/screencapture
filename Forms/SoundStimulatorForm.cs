﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TagLib.Mpeg;

namespace screencapture.Forms
{
    public partial class SoundStimulatorForm : Form
    {
        
        string pathToDirectory;
        string headline = "Звуковой стимулятор";
        int defaultPlaySoundTime;
        int currentPlaySoundTime;
        int delayTime;
        string[] files;
        int i;
        SoundPlayer soundPlayer;
        WaveFileReader audioFile;
        public SoundStimulatorForm(int defaultPlaySoundTime, int delayTime)
        {
            this.defaultPlaySoundTime = defaultPlaySoundTime*1000;
            this.delayTime = delayTime*1000;
            if (delayTime==0)
            {
                this.delayTime = 1;
            }
            InitializeComponent();
        }

        private void SoundStimulatorForm_Load(object sender, EventArgs e)
        {
            pathToDirectory = "d:\\Брендмашина\\screencapture\\screencapture\\bin\\Debug\\project\\stimulatorSounds";
        }

        private void timerSound_Tick(object sender, EventArgs e)
        {
            timerSound.Stop();
            i++;
            soundPlayer.Stop();
            if (i < files.Length)
            {
                timerDelay.Start();
            }
            else
            {
                descriptionLabel.Text = "Исследование завершено";
                startButton.Text = "OK";
                descriptionLabel.Visible = true;
                startButton.Visible = true;
            }
        }

        private void timerDelay_Tick(object sender, EventArgs e)
        {
            timerDelay.Stop();
            audioFile = new WaveFileReader(files[i]);
            currentPlaySoundTime = Convert.ToInt32(audioFile.TotalTime.TotalMilliseconds);
            if (currentPlaySoundTime < defaultPlaySoundTime)
            {
                timerSound.Interval = currentPlaySoundTime;
            }
            else
            {
                timerSound.Interval = defaultPlaySoundTime;
            }
            soundPlayer = new SoundPlayer(files[i]);
            soundPlayer.Play();
            
            
            timerSound.Start();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (startButton.Text == "OK")
            {
                this.Close();
            }
            else
            {
                descriptionLabel.Visible = false;
                startButton.Visible = false;
                files = Directory.GetFiles(pathToDirectory);
                timerDelay.Interval = delayTime;
                //timerDelay.Stop();
                audioFile = new WaveFileReader(files[i]);
                currentPlaySoundTime = Convert.ToInt32(audioFile.TotalTime.TotalMilliseconds);
                if (currentPlaySoundTime < defaultPlaySoundTime)
                {
                    timerSound.Interval = currentPlaySoundTime;
                }
                else
                {
                    timerSound.Interval = defaultPlaySoundTime;
                }
                soundPlayer = new SoundPlayer(files[i]);
                soundPlayer.Play();


                timerSound.Start();
            }
        }
    }
}
