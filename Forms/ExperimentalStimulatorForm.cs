﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class ExperimentalStimulatorForm : Form
    {

        string FIO;
        string pathToDirectory;
        string[] emotionDirectories;
        int picturesPerEmotion;
        int currentPictureBoxWidth;
        int currentPictureBoxHeight;
        string headline = "Изображения-стимуляторы";
        string currentDirectory;
        Image currentImage;
        List<string> currentFiles = new List<string>();
        int currentDirectoryIndex=0;
        int showPictureTime;
        int delayTime;
        //int opacityTimeIn = 1000;
        //int opacityTimeOut = 1000;
        // float fpsIn = 10;
        //float fpsOut = 10;
        //int opacityError = 0;
        //float currentOpacity = 0;
        List<Label> headerList = new List<Label>();
        int i;
        List<string> files=new List<string>();
        string currentImagePath;
        Dictionary<int, string> helplines = new Dictionary<int, string>();


        public ExperimentalStimulatorForm(int picturesPerEmotion, int showPictureTime, int delayTime, string FIO, string pathToDirectory)
        {
            this.picturesPerEmotion = picturesPerEmotion;
            this.FIO = FIO;
            this.showPictureTime = showPictureTime * 1000;
            this.delayTime = delayTime * 1000;
            this.pathToDirectory = pathToDirectory;
            if (delayTime == 0)
            {
                this.delayTime = 1;
            }
            if (showPictureTime == 0)
            { this.showPictureTime = 1; }
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            
            if (startButton.Text == "OK")
            {
                this.Close();
            }
            else
            {
                
                
                startButton.Visible = false;
                descriptionLabel.Visible = false;
                timerPictures.Interval = showPictureTime;
                timerDelay.Interval = delayTime;
                //timerOpacityIn.Interval = (int)(opacityTimeIn / fpsIn);
                //timerOpacityOut.Interval = (int)(opacityTimeOut / fpsOut);
                currentDirectory = emotionDirectories[currentDirectoryIndex];
                currentFiles = Directory.GetFiles(currentDirectory).ToList();
                for (int j = 0; j < picturesPerEmotion; j++)
                {
                    if (currentFiles.Count == 0)
                    { break; }
                    Random rand = new Random();
                    int k = rand.Next(0, currentFiles.Count);
                    files.Add(currentFiles[k]);
                    currentFiles.RemoveAt(k);

                }
                
                pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;

                //pictureBox.Visible = true;
                try
                {
                    showPicture(files[i]);
                }
                catch(ArgumentOutOfRangeException)
                {
                    MessageBox.Show("В папке " + currentDirectory + " нет изображений");
                    currentDirectoryIndex++;
                    if (currentDirectoryIndex < emotionDirectories.Length)
                    {
                        pictureBox.Visible = false;
                        descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                        descriptionLabel.Text = "Исследование приостановлено";
                        startButton.Text = "Продолжить";
                        descriptionLabel.Visible = true;
                        startButton.Visible = true;
                    }
                    else
                    {
                        pictureBox.Visible = false;
                        descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                        descriptionLabel.Text = "Исследование завершено";
                        startButton.Text = "OK";
                        descriptionLabel.Visible = true;
                        startButton.Visible = true;
                    }

                    
                }

            }
        }

        private void ExperimentalStimulatorForm_Load(object sender, EventArgs e)// Загружает путь к папке с картинками
        {
            currentPictureBoxHeight = Convert.ToInt32(this.Height * 0.8);
            currentPictureBoxWidth = Convert.ToInt32(this.Width * 0.8);
            emotionDirectories = Directory.GetDirectories(pathToDirectory);
            this.WindowState = FormWindowState.Maximized;

            //CreateHeader(32, 1);

        }
        
        private void SetPictureBoxPosition()
        {
            Size size = new Size(currentPictureBoxWidth, currentPictureBoxHeight);
            pictureBox.Size = size;
            int y = (this.ClientSize.Height - currentPictureBoxHeight) / 2;
            int x = (this.ClientSize.Width-currentPictureBoxWidth)/2;
            pictureBox.Location = new Point(x, y);


        }
        
        private void ExperimentalStimulatorForm_Resize(object sender, EventArgs e) //устанавливает позицию для слов и подсказок при резайзе формы
        {
            currentPictureBoxHeight = Convert.ToInt32(this.Height * 0.8);
            try
            {
                currentPictureBoxWidth = currentPictureBoxHeight * currentImage.Width / currentImage.Height;
            }
            catch(NullReferenceException)
            {
                currentPictureBoxWidth= Convert.ToInt32(this.Width * 0.8);
            }
            if (currentPictureBoxWidth > this.Width * 0.8)
            {
                currentPictureBoxWidth = Convert.ToInt32(this.Width * 0.8);
                currentPictureBoxHeight = currentPictureBoxWidth * currentImage.Height / currentImage.Width;
            }
            SetPictureBoxPosition();
            //setHelpLinesPosition(headerList);
            descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
            startButton.Location = new Point(this.ClientSize.Width / 2 - startButton.Width / 2, this.ClientSize.Height - startButton.Height - 5);
        }

        private void timerPictures_Tick(object sender, EventArgs e)
        {

            timerPictures.Stop();
            hidePicture();

        }
        private void timerDelay_Tick(object sender, EventArgs e)
        {
            timerDelay.Stop();
            i++;
            if (i<files.Count)
            {
                showPicture(files[i]);
            }
            else
            {
                currentDirectoryIndex++;
                if (currentDirectoryIndex < emotionDirectories.Length)
                {
                    files.Clear();
                    i = 0;
                    descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                    descriptionLabel.Text = "Исследование приостановлено";
                    startButton.Text = "Продолжить";
                    descriptionLabel.Visible = true;
                    startButton.Visible = true;
                }
                else
                {
                    descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                    descriptionLabel.Text = "Исследование завершено";
                    startButton.Text = "OK";
                    descriptionLabel.Visible = true;
                    startButton.Visible = true;
                }
            }
            

        }
        private void showPicture(string path)//TODO: добавть начало записи данных с шапки и веб-камеры в отдельные файлы
        {
            currentImagePath = path;
            currentImage= Image.FromFile(path);
            currentPictureBoxHeight =Convert.ToInt32(this.Height * 0.8);
            currentPictureBoxWidth = currentPictureBoxHeight * currentImage.Width / currentImage.Height;
            if (currentPictureBoxWidth > this.Width * 0.8)
            {
                currentPictureBoxWidth = Convert.ToInt32(this.Width * 0.8);
                currentPictureBoxHeight = currentPictureBoxWidth * currentImage.Height / currentImage.Width;
            }
            SetPictureBoxPosition();
            pictureBox.Image = Image.FromFile(path);
            pictureBox.Visible = true;
            
            /* 
             recording.start()
             */
            timerPictures.Start();

        }
        private void hidePicture()//TODO: добавть окончание записи данных с шапки и веб-камеры и сохранение данных
        {
            pictureBox.Visible = false;
            
            /*
             recording.Stop()
             */
            timerDelay.Start();
        }

        /*private void CreateHeader(int textSize, int position) //выводит хедеры на форму
                {
                    Label header = new Label();
                    header.Text = headline;
                    header.Font = new Font("Arial", textSize);
                    header.Width = this.ClientSize.Width;
                    header.TextAlign = ContentAlignment.TopCenter;
                    header.Visible = false;
                    header.Size = new Size(header.Size.Width, Convert.ToInt32(textSize * 1.56));
                    if (position < 1)
                    {
                        header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.02));
                    }
                    else
                    {
                        header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - header.Size.Height));
                    }


                    headerList.Add(header);

                    this.Controls.Add(header);
                }*/
        /*   private void setHelpLinesPosition(List<Label> headerList) //корректирует позиции подсказок
        {
            foreach (Label currentWord in headerList)
            {
                currentWord.Width = this.ClientSize.Width;
            }

            try
            {
                headerList[headerList.Count - 1].Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - headerList[headerList.Count - 1].Size.Height));

            }
            catch { }
        }*/

        /* ДЛЯ ПОСТЕПЕННОГО ПОЯВЛЕНИЯ/ИСЧЕЗНОВЕНИЯ КАРТИНОК
            private void timerPictures_Tick(object sender, EventArgs e)
            {
                pictureBox.Visible = true;
                timerPictures.Stop();
                hidePicture(files[i]);

            }
            private void timerDelay_Tick(object sender, EventArgs e)
            {
                timerDelay.Stop();
                pictureBox.Visible = true;
                showPicture(files[i]);

            }
            private void showPicture(string path)
            {
                pictureBox.Visible = true;
                timerOpacityIn.Start();


            }
            private void hidePicture(string path)
            {
                pictureBox.Visible = true;
                timerOpacityOut.Start();


            }
            public static Image SetImgOpacity(Image imgPic, float imgOpac)

            {

                Bitmap bmpPic = new Bitmap(imgPic.Width, imgPic.Height);
                Graphics gfxPic = Graphics.FromImage(bmpPic);
                ColorMatrix cmxPic = new ColorMatrix();
                cmxPic.Matrix33 = imgOpac;
                ImageAttributes iaPic = new ImageAttributes();
                iaPic.SetColorMatrix(cmxPic, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                gfxPic.DrawImage(imgPic, new Rectangle(0, 0, bmpPic.Width, bmpPic.Height), 0, 0, imgPic.Width, imgPic.Height, GraphicsUnit.Pixel, iaPic);
                gfxPic.Dispose();

                return bmpPic;

            }
            public static Bitmap ChangeOpacity(Image img, float opacityvalue)
            {
                Bitmap bmp = new Bitmap(img.Width, img.Height); // Determining Width and Height of Source Image
                Graphics graphics = Graphics.FromImage(bmp);
                ColorMatrix colormatrix = new ColorMatrix();
                colormatrix.Matrix33 = opacityvalue;
                ImageAttributes imgAttribute = new ImageAttributes();
                imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                graphics.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
                graphics.Dispose();   // Releasing all resource used by graphics 
                return bmp;
            }

            private void timerOpacityIn_Tick(object sender, EventArgs e)
            {
                currentOpacity += 1000 / fpsIn / opacityTimeIn;
                pictureBox.Image = ChangeOpacity(Image.FromFile(files[i]), currentOpacity);
                if (currentOpacity >= 1)
                {
                    timerOpacityIn.Stop();
                    currentOpacity = 1;
                    timerPictures.Start();
                }
            }

            private void timerOpacityOut_Tick(object sender, EventArgs e)
            {
                currentOpacity -= 1000 / fpsOut / opacityTimeOut;
                pictureBox.Image = ChangeOpacity(Image.FromFile(files[i]), currentOpacity);
                if (currentOpacity <= 0)
                {
                    timerOpacityOut.Stop();
                    currentOpacity = 0;
                    pictureBox.Visible = false;
                    i++;
                    if (i < files.Count)
                    {

                        timerDelay.Start();
                    }
                    else
                    {
                        descriptionLabel.Text = "Исследование завершено";
                        startButton.Text = "OK";
                        descriptionLabel.Visible = true;
                        startButton.Visible = true;
                    }

                }
            }*/
    }
    }

