﻿using screencapture.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture
{
    public partial class FormRecordDialog : Form
    {
        VideoRecorder recorder;
        TextBox tbProjectDirectory;
        Button btnRecStop;
        Form form1;

        public static IsRecording isRecording = IsRecording.No;

        public FormRecordDialog(TextBox tbProjectDirectory, VideoRecorder recorder, Button btnRecStop, Form form1)
        {
            this.btnRecStop = btnRecStop;
            this.recorder = recorder;
            this.tbProjectDirectory = tbProjectDirectory;
            this.form1 = form1;
            InitializeComponent();
        }

        private void btnStartRec_Click(object sender, EventArgs e)
        {
           
            {
                if (!File.Exists(Path.Combine(tbProjectDirectory.Text, tbName.Text + ".avi")))
                {

                    btnStartRec.Enabled = false;
                    statusLabel.Text = "Подождите, идет подготовка оборудования...";
                    statusLabel.Visible = true;
                    loadBox.Visible = true;
                    this.Size = new Size(this.Size.Width, this.Size.Height + 40);

                    new Thread(() =>
                    {
                        Thread.CurrentThread.IsBackground = true;
                        if (recorder.ThinkGearStart())
                        {
                            if (cbOpenResearchObject.CheckState == CheckState.Checked)
                            {

                                string[] files = Directory.GetFiles(Path.Combine(tbProjectDirectory.Text, FormCreateProject.researchObjectCatalog));
                                try
                                {
                                    Process.Start(files[0]);
                                }
                                catch
                                { }
                            }
                            Thread.Sleep(50);
                            Form1.isRecording = IsRecording.Yes;
                            StartRecord();
                        }
                        else
                        {
                            MessageBox.Show("Ой, что-то пошло не так :(", "Eye Stream Capture");
                        }
                    }).Start();
                        if (radioButton2.Checked == true)
                            {
                            StimulatorPicturesForm simulatorPicturesForm = new StimulatorPicturesForm(Convert.ToInt32(showTimeNumericUpDown.Value), Convert.ToInt32(delayNumericUpDown.Value), FIOtextBox.Text);
                            simulatorPicturesForm.Show();
                            }
                        if (radioButton3.Checked==true)
                        {
                            VideoStimulatorForm videoStimulatorForm=new VideoStimulatorForm(Convert.ToInt32(videoTimeNumericUpDown.Value), Convert.ToInt32(videoDelayNumericUpDown.Value));
                            videoStimulatorForm.Show();
                        }
                        if (radioButton4.Checked == true)
                        {
                            PictureExclusionForm pictureExclusionForm = new PictureExclusionForm();
                            pictureExclusionForm.Show();
                        }
                        if (radioButton5.Checked == true)
                        {
                            SoundStimulatorForm soundStimulatorForm = new SoundStimulatorForm(Convert.ToInt32(soundTimeNumericUpDown.Value), Convert.ToInt32(delaySoundNumericUpDown.Value));
                            soundStimulatorForm.Show();
                        }
                        if (radioButton6.Checked == true)
                        {
                            BlackBoxStimulator blackBoxStimulator = new BlackBoxStimulator(4,3,false, "");
                        blackBoxStimulator.Show();
                        }
                    if (radioButton7.Checked == true)
                    {
                        WebcamTestingForm webcamTestingForm = new WebcamTestingForm();
                        webcamTestingForm.Show();
                    }
                }
                else { MessageBox.Show("Файл с таким именем уже существует!"); }
            }
            
        }
        
        private void StartRecord(){
            try
            {
                BeginInvoke(new MethodInvoker(delegate
                {
                    SynchronizationContext uiContext = SynchronizationContext.Current;

                    //начинает запись
                    btnRecStop.Text = "СТОП";
                    recorder.StartRecording(tbProjectDirectory.Text, Path.Combine(tbProjectDirectory.Text, tbName.Text), tbName.Text, tbDescription.Text);
                    this.Close();
                    form1.WindowState = FormWindowState.Minimized;
                }));
            }
            catch
            {

            }
        }
        
        private void btnCancelRD_Click(object sender, EventArgs e)
        {
            this.Close();
            recorder.ThinkGearStop();
        }

        private void FormRecordDialog_Load(object sender, EventArgs e)
        {
            //TODO: автоматическое добавлениие к имени по умолчанию числа 
            //в формате {##} 01, 02... если уже существует файл с таким же именем
            string examinee = "examinee";
            tbName.Text = examinee;

            string[] portList = SerialPort.GetPortNames();
            foreach (string port in portList)
            { comboBox1.Items.Add(port); }
            
                //comboBox1.SelectedItem = comboBox1.Items[0];
            
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                pictureSettingsGroupBox.Enabled = true;
            }
            else
            {
                pictureSettingsGroupBox.Enabled = false;
            }
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton5.Checked == true)
            {
                soundSettingsGroupBox.Enabled = true;
            }
            else
            {
                soundSettingsGroupBox.Enabled = false;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton3.Checked == true)
            {
                videoSettingsGroupBox.Enabled = true;
            }
            else
            {
                videoSettingsGroupBox.Enabled = false;
            }
        }

        private void browserButton_Click(object sender, EventArgs e)
        {
            BrowserTestingForm browserTestingForm = new BrowserTestingForm();
            browserTestingForm.Show();
        }

        private void buttonExperimentalStimulator_Click(object sender, EventArgs e)
        {
            ExperimentalStimulatorForm experimentalStimulatorForm = new ExperimentalStimulatorForm(5,3,1,"testSubject","d:\\Брендмашина\\Изображения");
            experimentalStimulatorForm.Show();
        }




        private void button1_Click(object sender, EventArgs e)//для стимулятора черного ящика
        {
            if (comboBox1.Text != "")
            {
                BlackBoxStimulator blackBoxStimulator = new BlackBoxStimulator(4, 3, false, comboBox1.Text);
                blackBoxStimulator.Show();
            }
            else
            {
                MessageBox.Show("Вы не выбрали COM-порт!");
            }
        }
    }
}
