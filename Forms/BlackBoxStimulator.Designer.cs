﻿namespace screencapture.Forms
{
    partial class BlackBoxStimulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.descPanel = new System.Windows.Forms.Panel();
            this.tipLabel = new System.Windows.Forms.Label();
            this.nextButton = new System.Windows.Forms.Button();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.showTimer = new System.Windows.Forms.Timer(this.components);
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.descPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // descPanel
            // 
            this.descPanel.Controls.Add(this.tipLabel);
            this.descPanel.Controls.Add(this.nextButton);
            this.descPanel.Controls.Add(this.descriptionLabel);
            this.descPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descPanel.Location = new System.Drawing.Point(210, 12);
            this.descPanel.Name = "descPanel";
            this.descPanel.Size = new System.Drawing.Size(1165, 533);
            this.descPanel.TabIndex = 0;
            // 
            // tipLabel
            // 
            this.tipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tipLabel.Location = new System.Drawing.Point(134, 194);
            this.tipLabel.Name = "tipLabel";
            this.tipLabel.Size = new System.Drawing.Size(895, 103);
            this.tipLabel.TabIndex = 2;
            this.tipLabel.Text = "Нажмите на кнопку \"Начать исследование\", когда будете готовы";
            this.tipLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(439, 446);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(272, 87);
            this.nextButton.TabIndex = 1;
            this.nextButton.Text = "Начать исследование";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLabel.Location = new System.Drawing.Point(142, 0);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(887, 194);
            this.descriptionLabel.TabIndex = 0;
            this.descriptionLabel.Text = "Сейчас вам предстоит осмотреть представленные предметы";
            this.descriptionLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // showTimer
            // 
            this.showTimer.Tick += new System.EventHandler(this.showTimer_Tick);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(13, 127);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(179, 93);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // BlackBoxStimulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1551, 575);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.descPanel);
            this.Name = "BlackBoxStimulator";
            this.Text = "BlackBoxStimulator";
            this.Load += new System.EventHandler(this.BlackBoxStimulator_Load);
            this.Resize += new System.EventHandler(this.BlackBoxStimulator_Resize);
            this.descPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel descPanel;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Label tipLabel;
        private System.Windows.Forms.Timer showTimer;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}