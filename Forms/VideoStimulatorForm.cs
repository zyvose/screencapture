﻿using Microsoft.DirectX.AudioVideoPlayback;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class VideoStimulatorForm : Form
    {
        Video video;
        
        string pathToDirectory;
        string headline = "Видеостимуляторы";
        int defaultShowVideoTime;
        int currentShowVideoTime;
        int delayTime = 5000;
        List<Label> headerList = new List<Label>();
        int i;
        string[] files;

        public void OwnerVideo()
        {
            video.Owner = pnlVideo;
        }
        public VideoStimulatorForm(int showVideoTime, int delayTime)
        {
            
            this.defaultShowVideoTime = showVideoTime*1000;
            this.delayTime = delayTime*1000;
            if (delayTime == 0)
            {
                this.delayTime = 1;
            }
            InitializeComponent();
        }

        private void VideoStimulatorForm_Load(object sender, EventArgs e)
        {
            pathToDirectory = "d:\\Брендмашина\\screencapture\\screencapture\\bin\\Debug\\project\\stimulatorVideos";
            this.WindowState = FormWindowState.Maximized;
            CreateHeader(32, 1);
        }
        private void CreateHeader(int textSize, int position) //выводит хедеры на форму
        {
            Label header = new Label();
            header.Text = headline;
            header.Font = new Font("Arial", textSize);
            header.Width = this.ClientSize.Width;
            header.TextAlign = ContentAlignment.TopCenter;
            header.Visible = false;
            header.Size = new Size(header.Size.Width, Convert.ToInt32(textSize * 1.56));
            if (position < 1)
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.02));
            }
            else
            {
                header.Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - header.Size.Height));
            }


            headerList.Add(header);

            this.Controls.Add(header);
        }
        private void setHelpLinesPosition(List<Label> headerList) //корректирует позиции подсказок
        {
            foreach (Label currentWord in headerList)
            {
                currentWord.Width = this.ClientSize.Width;
            }

            try
            {
                headerList[headerList.Count - 1].Location = new Point(0, Convert.ToInt32(this.ClientSize.Height * 0.98 - headerList[headerList.Count - 1].Size.Height));

            }
            catch { }
        }
        private void timerVideo_Tick(object sender, EventArgs e)
        {
            timerVideo.Stop();
            i++;
            video.Stop();
            video = null;
            pnlVideo.Visible = false;
            if (i< files.Length)
            {
                timerDelay.Start();
            }
            else
            {
                descriptionLabel.Text = "Исследование завершено";
                startButton.Text = "OK";
                descriptionLabel.Visible = true;
                startButton.Visible = true;
            }
        }

        private void timerDelay_Tick(object sender, EventArgs e)
        {
            timerDelay.Stop();
            video= new Video(files[i]);
            video.Owner = pnlVideo;
            
            currentShowVideoTime = Convert.ToInt32(video.Duration*1000);
            if (currentShowVideoTime < defaultShowVideoTime)
            {
                timerVideo.Interval = currentShowVideoTime;
            }
            else
            {
                timerVideo.Interval = defaultShowVideoTime;
            }
            FullScreenVideo();
            pnlVideo.Visible = true;
            video.Play();


            timerVideo.Start();
        }
        public void FullScreenVideo()
        {
            pnlVideo.Size = new Size(this.ClientSize.Width, this.ClientSize.Height);
            pnlVideo.Location = new Point(0, 0);
            
        }
        private void startButton_Click(object sender, EventArgs e)
        {
            if (startButton.Text == "OK")
            {
                this.Close();
            }
            else
            {
                descriptionLabel.Visible = false;
                startButton.Visible = false;
                timerDelay.Interval = delayTime;
                
                files = Directory.GetFiles(pathToDirectory);
                //timerDelay.Stop();
                video = new Video(files[i]);
                video.Owner = pnlVideo;
                currentShowVideoTime = Convert.ToInt32(video.Duration*1000);
                if (currentShowVideoTime < defaultShowVideoTime)
                {
                    timerVideo.Interval = currentShowVideoTime;
                }
                else
                {
                    timerVideo.Interval = defaultShowVideoTime;
                }
                FullScreenVideo();
                video.Play();


                timerVideo.Start();
            }
        }

        private void VideoStimulatorForm_Resize(object sender, EventArgs e)
        {
            
            setHelpLinesPosition(headerList);
            descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
            startButton.Location = new Point(this.ClientSize.Width / 2 - startButton.Width / 2, this.ClientSize.Height - startButton.Height - 5);
            if (video != null || i > 0)
            {
                FullScreenVideo();
            }
        }
    }
}
