﻿namespace screencapture.Forms
{
    partial class ExperimentalStimulatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startButton = new System.Windows.Forms.Button();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.timerPictures = new System.Windows.Forms.Timer(this.components);
            this.timerDelay = new System.Windows.Forms.Timer(this.components);
            this.timerOpacityIn = new System.Windows.Forms.Timer(this.components);
            this.timerOpacityOut = new System.Windows.Forms.Timer(this.components);
            this.pictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startButton.Location = new System.Drawing.Point(679, 483);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(303, 45);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Начать показ";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLabel.Location = new System.Drawing.Point(572, 9);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(534, 46);
            this.descriptionLabel.TabIndex = 2;
            this.descriptionLabel.Text = "Изображения-стимуляторы";
            // 
            // timerPictures
            // 
            this.timerPictures.Tick += new System.EventHandler(this.timerPictures_Tick);
            // 
            // timerDelay
            // 
            this.timerDelay.Tick += new System.EventHandler(this.timerDelay_Tick);
            // 
            // timerOpacityIn
            // 
            
            // 
            // timerOpacityOut
            // 
           
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(266, 79);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1041, 366);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // ExperimentalStimulatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1496, 540);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.startButton);
            this.Name = "ExperimentalStimulatorForm";
            this.Text = "ExperimentalStimulatorForm";
            this.Load += new System.EventHandler(this.ExperimentalStimulatorForm_Load);
            this.Resize += new System.EventHandler(this.ExperimentalStimulatorForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Timer timerPictures;
        private System.Windows.Forms.Timer timerDelay;
        private System.Windows.Forms.Timer timerOpacityIn;
        private System.Windows.Forms.Timer timerOpacityOut;
    }
}