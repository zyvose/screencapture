﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace screencapture.Forms
{
    
    public partial class BrowserTestingForm : Form
    {
        
        string firstPicsDiretory="D:\\Брендмашина\\1\\";
        string secondPicsDiretory = "D:\\Брендмашина\\2\\";
        string finalPicsDiretory = "D:\\Брендмашина\\3\\";
        string address;
        string[] pics = new string[20];
        int count = 0;
        int number = -1;
        bool wasSelected = false;
        Bitmap testPicture;
        List<string> files;
        List<string> startFiles;
        List<string> finishFiles;
        WebClient web = new WebClient();
        string request;
        string testerName="vladimir";
        string emotion="testEmotion";
        List<Image> testImages=new List<Image>();
        //List<string> filesToRemove;
        //string testString;
        string searchAddress;
        int pictureWidth = 200;
        int pictureHeight = 200;
        int pictureMargin = 10;
        List<PictureBox> pictureList = new List<PictureBox>();
        public BrowserTestingForm()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            

        }

                
        private void searchButton_Click(object sender, EventArgs e)
        {
            searchButton.Enabled = false;
            request = textBox1.Text;
            if (files != null)
            { files.Clear(); }
            if (finishFiles != null)
            { finishFiles.Clear(); }
            if (pictureList != null)
            { pictureList.Clear(); }
            if (imageList1.Images != null)
            { imageList1.Images.Clear(); }
            if (testImages != null)
            { testImages.Clear(); }
            
          
            listView1.Clear();
            foreach(string file in Directory.GetFiles(firstPicsDiretory).ToList())
            {
                try
                { File.Delete(file); }
                catch { }
            }
            foreach (string file in Directory.GetFiles(secondPicsDiretory).ToList())
            {
                File.Delete(file);
            }
            searchAddress = "https://www.google.com/search?q=" + textBox1.Text + "&safe=strict&source=lnms&tbm=isch";
           
            ParseImages(searchAddress);
        }
        public void ParseImages(string address)
        {
            this.address = address;
            webBrowser1.Navigate(address);
            
        }

        private void CreatePictureObjects(List<string> pathToDirectory)
        {
            foreach (string file in pathToDirectory)
            {
                try
                {
                    using (var fs = File.Open(file, FileMode.Open))
                        testImages.Add(new Bitmap(fs));
                    
                }
                catch (ArgumentException)
                { }
               
            }
            listView1.View = View.LargeIcon;
            foreach (var image in testImages)
            {
                imageList1.Images.Add(image);
            }
            
            imageList1.ImageSize = new Size(pictureWidth,pictureHeight);
            listView1.LargeImageList = this.imageList1;
            ListViewItem item;
            for (int i = 0; i < this.imageList1.Images.Count; i++)
            {
                item = new ListViewItem();
                item.ImageIndex = i;
                item.Text = "";
                listView1.Items.Add(item);
                
            }

            
        }
        
        private void SelectPictureObject(object sender, EventArgs e)
        {
            
                for(int j=0;j<pictureList.Count;j++)
                {
                    if (sender == pictureList[j])
                    {
                        SelectPictureForm selectPictureForm = new SelectPictureForm(files[j],request,testerName,emotion);
                        selectPictureForm.Show();
                        
                        break;
                    }
                }

          
        }

        private void SetPositionPictureObjects(List<PictureBox> pictureList)
        {
            int leftPadding = Convert.ToInt32(this.ClientSize.Width * 0.1);
            int topPadding = 100;

            int x = leftPadding;
            int y = topPadding;

            foreach (PictureBox currentPicture in pictureList)
            {
                currentPicture.Visible = true;
                currentPicture.Location = new Point(x, y);
                this.Controls.Add(currentPicture);
                if (x + pictureMargin + leftPadding + currentPicture.Size.Width * 2 < this.ClientSize.Width)
                    x = x + currentPicture.Size.Width + pictureMargin;
                else
                {
                    x = leftPadding;
                    y = y + currentPicture.Size.Height + 10;
                }
            }

            //CenterLabelAlignHorizontal(labelList);
            //CenterLabelAlignVertical(labelList);
        }
        private void CenterPictureAlignHorizontal(List<PictureBox> pictureList) 
        {
            if (pictureList.Count > 0)
            {
                PictureBox currentPicture = pictureList[0];
                int id = 0;

                foreach (PictureBox currentWord in pictureList)
                {
                    if (((id + 1) == pictureList.Count) || (pictureList[id + 1].Location.Y != currentPicture.Location.Y))
                    {
                        int newLocation = (Convert.ToInt32(this.ClientSize.Width - ((pictureList[id].Location.X + pictureList[id].Size.Width) - currentPicture.Location.X)) / 2) - currentPicture.Location.X;
                        for (int i = pictureList.IndexOf(currentPicture); i <= id; i++)
                        {
                            pictureList[i].Location = new Point(pictureList[i].Location.X + newLocation, pictureList[i].Location.Y);
                        }
                        if ((id + 1) != pictureList.Count)
                            currentPicture = pictureList[id + 1];
                    }
                    id++;
                }
            }
        }
        static string getResponse(string uri)
        {
            StringBuilder sb = new StringBuilder();
            byte[] buf = new byte[8192];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();
            int count = 0;
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    sb.Append(Encoding.Default.GetString(buf, 0, count));
                }
            }
            while (count > 0);
            return sb.ToString();
        }
        public static String GetCode(string urlAddress)
        {
            //string urlAddress = "http://google.com";
            string data = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;
                if (response.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }
                data = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
            }
            return data;
        }
        public static void DownLoadFileInBackground2(string address)
        {
            WebClient wc = new WebClient();
            string path = address;
            try
            {
                wc.DownloadFileAsync(new Uri(path), "D:\\Брендмашина\\1\\"+ Path.GetFileName(path));
            }
            catch { }
            

        }

        

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                number = listView1.SelectedIndices[0];
            }
            else { wasSelected = false; }
        }

        private void listView1_MouseUp(object sender, MouseEventArgs e)
        {

            if (listView1.SelectedIndices.Count==1&& ((listView1.SelectedIndices[0]!=number)||(wasSelected==false)))
            {
                count = listView1.SelectedIndices[0];
                richTextBox1.Text += listView1.SelectedIndices[0]+"\n\n";
                SelectPictureForm selectPictureForm = new SelectPictureForm(Directory.GetFiles(secondPicsDiretory)[count],request,testerName,emotion);
                selectPictureForm.Show();
                
            }
        }

        

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
             if (e.Url.AbsolutePath == (sender as WebBrowser).Url.AbsolutePath)
                {
                string htmlCode = "";
                
                htmlCode = webBrowser1.Document.Body.InnerHtml;

                
                richTextBox1.Text += address + "\n";
                

                List<int> indexes = STR.AllIndexesOf(htmlCode, "\"ou\"");

                
                richTextBox1.Text += indexes.Count();

                for (int j = 0; j < pics.Count(); j++)
                {
                    try
                    {
                        pics[j] = htmlCode.Substring(indexes[j] + 6);

                        int endIndex = pics[j].IndexOf("\",\"ow");
                        pics[j] = pics[j].Substring(0, endIndex);
                        if (pics[j].Contains("focal("))
                        {
                            int startIndex = pics[j].IndexOf(")/") + 2;
                            pics[j] = pics[j].Substring(0, 8) + pics[j].Substring(startIndex);
                        }
                        

                        DownLoadFileInBackground2(pics[j]);

                    }
                    catch { }
                }

                startFiles = Directory.GetFiles(firstPicsDiretory).ToList();


               
                foreach (var file in startFiles)
                {
                    try
                    {
                        using (var fs = File.Open(file, FileMode.Open))
                            testPicture = new Bitmap(fs);
                        File.Copy(file, secondPicsDiretory+ Path.GetFileName(file));
                    }
                    catch(Exception ex)
                    {
                        //Thread.Sleep(500);
                        //MessageBox.Show(ex.Message);

                    }

                }




                
                finishFiles = Directory.GetFiles(secondPicsDiretory).ToList();
                richTextBox1.Text += "\n\n\n";

                finishFiles = Directory.GetFiles(secondPicsDiretory).ToList();
                CreatePictureObjects(finishFiles);
                searchButton.Enabled = true;
                files = Directory.GetFiles(secondPicsDiretory).ToList();




                
            }
        }

        private void BrowserTestingForm_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchButton.Enabled == true && e.KeyCode == Keys.Enter)
            {
                searchButton.PerformClick();
            }
        }
    }
    
    static class STR
    { 
    public static List<int> AllIndexesOf(this string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            List<int> indexes = new List<int>();
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);
            }
            
        }
    }
}
