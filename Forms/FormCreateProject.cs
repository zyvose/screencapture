﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Xml.Serialization;

namespace screencapture.Forms
{
    public partial class FormCreateProject : Form
    {
        private TextBox tbForm1_projectDirectory;
        private ResearchType researchType = 0;

        private string pathToResearchObject;
        public static readonly string researchObjectCatalog = "ResearchObject";// название папки для объекта исследования

        public FormCreateProject(TextBox projectDirectory)
        {
            this.tbForm1_projectDirectory = projectDirectory;
            InitializeComponent();
        }
        
        public class projectType
        {
            public string name;
            public ResearchType type;
        }

        private void btnCreateProject_Click(object sender, EventArgs e)
        {
            string projectName = tbProjectName.Text;
            if (!String.IsNullOrWhiteSpace(projectName))
            {
                string pathToNewProject = Path.Combine(ConfigurationManager.AppSettings["Default_Project_Directory"], projectName);
                string pathToResearchObjectDirectory = Path.Combine(pathToNewProject, researchObjectCatalog);
                string extension = Path.GetExtension(pathToResearchObject);

                Directory.CreateDirectory(pathToNewProject);
                Directory.CreateDirectory(pathToResearchObjectDirectory);

                switch (researchType)
                {
                    case ResearchType.NoType:
                        break;
                    case ResearchType.LocalFile:
                        CreateProjectWithLocalFile(pathToResearchObjectDirectory, extension);
                        break;
                    case ResearchType.Website:
                        CreateProjectWithWebSite(pathToResearchObjectDirectory);
                        break;
                    case ResearchType.Naming:
                        CreateProjectWithLocalFile(pathToResearchObjectDirectory, extension);
                        //еще чето должно быть...
                        break;
                    default:
                        break;
                }

                projectType projectType = new projectType();

                    projectType.name = tbProjectName.Text;
                    projectType.type = researchType;

                using (Stream writer = new FileStream(pathToResearchObjectDirectory + "//type.xml", FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(projectType));
                    serializer.Serialize(writer, projectType);
                }

                tbForm1_projectDirectory.Text = pathToNewProject;
                this.Close();
            }
            else
            {
                MessageBox.Show("Вы не ввели имя проекта!");
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                switch (radioButton.Name)
                {
                    case "rbTypeWebsite":
                        researchType = ResearchType.Website;
                        tbFileDirectory.Enabled = false;
                        btnBrowseFile.Enabled = false;
                        tbWebRef.Enabled = true;
                        break;
                    case "rbTypeLocalFile":
                        researchType = ResearchType.LocalFile;
                        tbFileDirectory.Enabled = true;
                        btnBrowseFile.Enabled = true;
                        tbWebRef.Enabled = false;
                        break;
                    case "rbNoType":
                        researchType = ResearchType.NoType;
                        tbFileDirectory.Enabled = false;
                        btnBrowseFile.Enabled = false;
                        tbWebRef.Enabled = false;
                        break;
                    case "rbNamingType":
                        researchType = ResearchType.Naming;
                        tbFileDirectory.Enabled = false;
                        btnBrowseFile.Enabled = false;
                        tbWebRef.Enabled = false;
                        tbNamingDirectory.Enabled = true;
                        btnBrowseNaming.Enabled = true;
                        break;
                    default:
                        break;
                }
            }
        }

        private void btnBrowseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdSelectFile = new OpenFileDialog();
            pathToResearchObject = tbFileDirectory.Text = ofdSelectFile.ShowDialog() == DialogResult.OK ? ofdSelectFile.FileName : null;
        }

        private void CreateProjectWithLocalFile(string pathToResearchObjectDirectory, string extension)
        {
            string pathToNewFile = Path.Combine(pathToResearchObjectDirectory, researchObjectCatalog + extension);

            if (!File.Exists(pathToNewFile))
                File.Copy(pathToResearchObject, pathToNewFile);
        }

        private void CreateProjectWithWebSite(string pathToResearchObjectDirectory)
        {
            Uri uri;
            if (!Uri.TryCreate(tbWebRef.Text, UriKind.Absolute, out uri))
            {
                MessageBox.Show("Вы неправильно ввели URL-адресс");
            }
            else
            {
                UriBuilder ub = new UriBuilder(tbWebRef.Text);
                string nameUri = ub.Host;
                using (StreamWriter sw = new StreamWriter(Path.Combine(pathToResearchObjectDirectory, nameUri + ".url"), false, Encoding.ASCII))
                    sw.WriteLine("[InternetShortcut]\nURL=" + tbWebRef.Text);
            }
        }

        private void btnBrowseNaming_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdSelectFile = new OpenFileDialog();
            pathToResearchObject = tbNamingDirectory.Text = ofdSelectFile.ShowDialog() == DialogResult.OK ? ofdSelectFile.FileName : null;
        }

    }
}
