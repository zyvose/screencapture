﻿namespace screencapture.Forms
{
    partial class FormRecovery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.directoryProjectLabel = new System.Windows.Forms.Label();
            this.ProjectDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.projectList = new System.Windows.Forms.ListBox();
            this.examineeList = new System.Windows.Forms.ListBox();
            this.timeBox = new System.Windows.Forms.CheckBox();
            this.mouseBox = new System.Windows.Forms.CheckBox();
            this.mindBox = new System.Windows.Forms.CheckBox();
            this.eyeBox = new System.Windows.Forms.CheckBox();
            this.startButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // directoryProjectLabel
            // 
            this.directoryProjectLabel.AutoSize = true;
            this.directoryProjectLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.directoryProjectLabel.Location = new System.Drawing.Point(8, 9);
            this.directoryProjectLabel.Name = "directoryProjectLabel";
            this.directoryProjectLabel.Size = new System.Drawing.Size(136, 15);
            this.directoryProjectLabel.TabIndex = 10;
            this.directoryProjectLabel.Text = "Директория проектов";
            // 
            // ProjectDirectoryTextBox
            // 
            this.ProjectDirectoryTextBox.Enabled = false;
            this.ProjectDirectoryTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ProjectDirectoryTextBox.Location = new System.Drawing.Point(11, 28);
            this.ProjectDirectoryTextBox.Name = "ProjectDirectoryTextBox";
            this.ProjectDirectoryTextBox.ReadOnly = true;
            this.ProjectDirectoryTextBox.Size = new System.Drawing.Size(376, 21);
            this.ProjectDirectoryTextBox.TabIndex = 9;
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(393, 27);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(87, 23);
            this.browseButton.TabIndex = 11;
            this.browseButton.Text = "Обзор";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // projectList
            // 
            this.projectList.FormattingEnabled = true;
            this.projectList.Location = new System.Drawing.Point(12, 55);
            this.projectList.Name = "projectList";
            this.projectList.Size = new System.Drawing.Size(156, 121);
            this.projectList.TabIndex = 12;
            this.projectList.SelectedValueChanged += new System.EventHandler(this.projectList_SelectedValueChanged);
            // 
            // examineeList
            // 
            this.examineeList.FormattingEnabled = true;
            this.examineeList.Location = new System.Drawing.Point(174, 55);
            this.examineeList.Name = "examineeList";
            this.examineeList.Size = new System.Drawing.Size(213, 121);
            this.examineeList.TabIndex = 13;
            // 
            // timeBox
            // 
            this.timeBox.AutoSize = true;
            this.timeBox.Location = new System.Drawing.Point(393, 56);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(73, 17);
            this.timeBox.TabIndex = 14;
            this.timeBox.Text = "timestamp";
            this.timeBox.UseVisualStyleBackColor = true;
            // 
            // mouseBox
            // 
            this.mouseBox.AutoSize = true;
            this.mouseBox.Location = new System.Drawing.Point(393, 79);
            this.mouseBox.Name = "mouseBox";
            this.mouseBox.Size = new System.Drawing.Size(57, 17);
            this.mouseBox.TabIndex = 15;
            this.mouseBox.Text = "mouse";
            this.mouseBox.UseVisualStyleBackColor = true;
            // 
            // mindBox
            // 
            this.mindBox.AutoSize = true;
            this.mindBox.Location = new System.Drawing.Point(393, 102);
            this.mindBox.Name = "mindBox";
            this.mindBox.Size = new System.Drawing.Size(48, 17);
            this.mindBox.TabIndex = 16;
            this.mindBox.Text = "mind";
            this.mindBox.UseVisualStyleBackColor = true;
            // 
            // eyeBox
            // 
            this.eyeBox.AutoSize = true;
            this.eyeBox.Location = new System.Drawing.Point(393, 125);
            this.eyeBox.Name = "eyeBox";
            this.eyeBox.Size = new System.Drawing.Size(43, 17);
            this.eyeBox.TabIndex = 17;
            this.eyeBox.Text = "eye";
            this.eyeBox.UseVisualStyleBackColor = true;
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startButton.Location = new System.Drawing.Point(393, 154);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(87, 23);
            this.startButton.TabIndex = 18;
            this.startButton.Text = "Собрать";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // FormRecovery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 189);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.eyeBox);
            this.Controls.Add(this.mindBox);
            this.Controls.Add(this.mouseBox);
            this.Controls.Add(this.timeBox);
            this.Controls.Add(this.examineeList);
            this.Controls.Add(this.projectList);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.directoryProjectLabel);
            this.Controls.Add(this.ProjectDirectoryTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormRecovery";
            this.Text = "Eye Stream Capture Recovery Mode";
            this.Load += new System.EventHandler(this.FormRecovery_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label directoryProjectLabel;
        private System.Windows.Forms.TextBox ProjectDirectoryTextBox;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.ListBox projectList;
        private System.Windows.Forms.ListBox examineeList;
        private System.Windows.Forms.CheckBox timeBox;
        private System.Windows.Forms.CheckBox mouseBox;
        private System.Windows.Forms.CheckBox mindBox;
        private System.Windows.Forms.CheckBox eyeBox;
        private System.Windows.Forms.Button startButton;
    }
}