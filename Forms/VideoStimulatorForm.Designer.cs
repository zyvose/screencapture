﻿namespace screencapture.Forms
{
    partial class VideoStimulatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.timerVideo = new System.Windows.Forms.Timer(this.components);
            this.timerDelay = new System.Windows.Forms.Timer(this.components);
            this.pnlVideo = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLabel.Location = new System.Drawing.Point(239, 9);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(879, 73);
            this.descriptionLabel.TabIndex = 0;
            this.descriptionLabel.Text = "Видеостимулятор";
            this.descriptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startButton.Location = new System.Drawing.Point(591, 419);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(226, 67);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Начать показ";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // timerVideo
            // 
            this.timerVideo.Tick += new System.EventHandler(this.timerVideo_Tick);
            // 
            // timerDelay
            // 
            this.timerDelay.Tick += new System.EventHandler(this.timerDelay_Tick);
            // 
            // pnlVideo
            // 
            this.pnlVideo.Location = new System.Drawing.Point(308, 76);
            this.pnlVideo.Name = "pnlVideo";
            this.pnlVideo.Size = new System.Drawing.Size(751, 313);
            this.pnlVideo.TabIndex = 2;
            // 
            // VideoStimulatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1487, 498);
            this.Controls.Add(this.pnlVideo);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.descriptionLabel);
            this.Name = "VideoStimulatorForm";
            this.Text = "VideoStimulatorForm";
            this.Load += new System.EventHandler(this.VideoStimulatorForm_Load);
            this.Resize += new System.EventHandler(this.VideoStimulatorForm_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Timer timerVideo;
        private System.Windows.Forms.Timer timerDelay;
        private System.Windows.Forms.Panel pnlVideo;
    }
}